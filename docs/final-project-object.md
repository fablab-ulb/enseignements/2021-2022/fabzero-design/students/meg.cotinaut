# MODE D'EMPLOI

Sur cette page, vous pouvez retrouver un court résumé de la notice de fabrication et du mode d'emploi de l'objet final.

![](./images/projet-final/test-final/image_finale_objet.jpg)

## **L'OBJET**

![](./images/projet-final/test-final/objet-final.jpg)

La pop'o box est un module qui se fixe partout et dans lequel on peut emboîter les éléments de notre choix afin de varier les usages et faciliter la vie sur la route dans le petit espace que représente un van. Il s'adapte non seulement au tableau de bord des VW T3, avec la possibilité de former une console à part entière en emboîtant plusieurs d'entre eux grâce aux aimants, pour avoir un accès rapide à tout ce dont on a besoin au volant en sécurité ; mais il s'accroche également un peu partout, à l'intérieur comme à l'extérieur du van. Par sa petite taille et son assemblage, il est peu encombrant, pratique lorsque chaque cm2 de notre si petit espace de vie doit être rentabilisé ! Les possibilités d'éléments à emboîter dans ce module sont nombreuses et peuvent répondre aux envies et besoins spécifiques de chacun dans leur aventure : le but de la pop'o box, c'est de proposer l'opportunité à tous les possesseurs de T3 de façonner leurs modules à leurs utilisations avec une économie de matériaux et une simplicité de mise en oeuvre dans un FabLab.

![](./images/projet-final/test-final/modules_fabrication.jpg)

<br>

## **NOTICE DE FABRICATION**

### **Étape 1 :** découpe des éléments

![](./images/projet-final/test-final/patron_boite_2.jpg)

### **Étape 2 :** assemblage avec tourillons

![](./images/projet-final/test-final/etape_1_tourrillons.jpg)

### **Étape 3 :** rondelles et aimants

![](./images/projet-final/test-final/aimants_placement.jpg)

![](./images/projet-final/test-final/etape_2_rondelles.jpg)

![](./images/projet-final/test-final/etape_3_aimants.jpg)

Modélisation du module et de différents éléments emboîtables disponible en suivant [ce lien](https://a360.co/3IG1KrN).
Dans cette modélisation, l'épaisseur du bois, la largeur et hauteur de la boite ainsi que l'épaisseur et le diamètre des aimants et rondelles sont paramétriques, il est donc possible de varier tous ces éléments selon vos besoins ou préférences.

<br>

## **MODE D'EMPLOI**

Dans le module, vous pouvez disposer les éléments emboîtables selon vos envies et besoins, ils peuvent être réalisés avec différentes techniques allant de l'impression 3D à la thermoformeuse pour vos objets propres, en passant par la découpe laser.

Les modélisations 3D de plusieurs éléments comme un porte-gobelet, un bol, un repose tasse et un double-fond, qui grâce aux paramètres d'épaisseur de bois et de largeur de la boîte, varieront selon vos choix de ces derniers, sont disponibles en suivant [le lien de la modélisation complète](https://a360.co/3IG1KrN).

<iframe src="https://myulb184.autodesk360.com/shares/public/SH9285eQTcf875d3c5390f90afbf8fbff472?mode=embed" width="800" height="600" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

N'hésitez pas à concevoir vos propres emboîtements :)

### **Console de tableau de bord**

Grâce à l'angle du module et son aimant du dessous, il est possible de le disposer sur le tableau de bord du VW T3 sans risquer qu'il glisse d'un côté ou de l'autre ou qu'il tombe. Grâce aux aimants et rondelles disposés sur chaque face, les modules sont emboîtables les uns avec les autres pour former une console à part entière dans laquelle disposer les éléments que l'on souhaite.

![](./images/projet-final/test-final/etape_4_modules.jpg)

![](./images/projet-final/test-final/etape_5_modules.jpg)

![](./images/projet-final/test-final/photo_tdb_module.jpg)

### **Dans le van**

Avec son aimant arrière, il est possible de fixer le module sur les parois intérieures en métal, ou alors de le poser sur un meuble, afin de l'utiliser pour différents usages de la vie quotidienne dans un van.

![](./images/projet-final/test-final/etape_7_int.jpg)

### **À l'extérieur**

De même, il est également possible de le fixer à la carrosserie du van pour profiter de son café au grand air le matin ou pour porter un outil !

![](./images/projet-final/test-final/etape_6_ext.jpg)

<br>

## L'histoire du nom

Pourquoi **Pop'o box** ? Le principe de ce module est d'avoir des petites boîtes de rangement qui s'accrochent partout et qui peuvent se 'pop !' ensemble, le surnom officiel du modèle T3 étant *Popo*... voilà la Pop'o box. Concept que l'on retrouve également dans son pictogramme, des cubes qui s'emboîtent pour faciliter le quotidien dans un van sur la route à suivre le soleil ! :)

<br>

# Liens utiles (à compléter)

- [Fichier .stl du module et ses compartiments](https://a360.co/3IG1KrN)
