# LE PROJET DE A à Z


Sur cette page, vous pouvez retrouver la conception entière de mon objet final au travers d'une documentation par semaine et chapitres, suivant les étapes expliquées dans le cours de *Design or not design* que vous pouvez trouver dans l'onglet **Cours Design**.

# **DÉFINITION PROJET**

Mon projet consiste en la création d'un support déplaçable permettant de maintenir des objets dans mon van, notamment au niveau du tableau de bord, donnant un accès rapide et simple à des objets pendant la conduite. L'aspect déplaçable repose sur le fait que dans un van, la place manque rapidement et l'organisation aide beaucoup à éviter l'encombrement dans un si petit espace, le rendre déplaçable permet lorsque l'on ne conduit pas, de pouvoir maintenir des objets et leur donner un accès rapide depuis l'espace de vie ou même à l'extérieur du van.

<br>

# **ÉTAPE 1 :** Déconstruction  de la fonction

Par petit groupe de 3, nous avons déconstruit la fonction de nos objets respectifs, les avis extérieurs permettant alors de réellement se détacher de notre objet et de l'idée que l'on s'en fait afin de prendre la place d'un simple observateur.

Pour ma console de tableau de bord, nous avons relevé plusieurs fonctions principales. La première étant de **ranger des objets divers en les rendant accessibles rapidement depuis le poste de conduite du van lorsque l'on roule**. La console permet alors de **maintenir** et d'offrir **une stabilité à ces objets en les gardant à portée de main** : sa fonction première est donc **de les tenir en place**. Sa fonction est également de **s'adapter** au tableau de bord existant car c'est un objet dont la fonction n'a pas de sens s'il est isolé, par lui même, mais qui vient exister lorsque l'objet s'ajoute à un autre : le tableau de bord d'une voiture.

**-> Accessibilité, stabilité et adaptabilité**

<br>

# **ÉTAPE 2 :** Résoudre un vrai problème

Toujours accompagnée de mon groupe, la seconde étape était donc, comme expliqué plus tôt, de comprendre les limites et contraintes qui vont structurer la conception de mon objet.

La contrainte principale que nous avons mise en avant est celle de **l'accessibilité** car il y a une question de sécurité routière, en effet la console permet de mettre à portée des mains des objets pour éviter de devoir quitter la route du regard en fouillant ou cherchant dans un sac, une portière ou une boîte à gants. Cette dernière impose également une contrainte au niveau de la taille de l'objet, qui, à cause de son positionnement sur le tableau de bord, **ne doit pas venir créer un obstacle dans la vision du conducteur**.

Il doit également être **adapté par rapport à la diversité des objets qu'on veut y ranger et qui prennent tous des formes et des dimensions différentes** (thermos ou téléphones par exemple), mais aussi **adaptable à plusieurs vans et utilisateurs**. Il faut pouvoir penser à de quoi ont besoin les vanlifers et qui rendrait cet objet diffusable et utile.

Une autre contrainte que nous avons relevé est celle liée au fait que par son emplacement sur le tableau de bord et donc dans la cabine avant, cette console sera **visible à tous**. Cela lève la question de lui donner une certaine polyvalence en permettant de ranger un objet certes, mais également de le cacher de la vue de tous, pour qu'il soit accessible lorsque l'on conduit mais qu'il puisse rester à sa place lorsque l'on quitte le van.

Finalement, il y a également la question du poids et de la forme, l'objet ayant pour contrainte de **venir se caler sur le tableau de bord** (donc pas trop lourd) **en gardant une stabilité et en restant bien fixe**, car la fonction de pouvoir garder les objets bien maintenus lorsque l'on conduit implique de créer un objet qui n'est pas non plus trop léger pour ne pas qu'il glisse ou se déplace au premier virage, mais aussi d'une forme permettant de le maintenir calé lors des déplacements.

Pour citer quelques unes des limites et faiblesses des consoles existantes qui découlent de nos échanges sur les contraintes de mon objet, on peut dire qu'elles manquent cruellement de possibilités et d'adaptabilité aux objets, par exemple celle choisie dans ma présentation ne peut accueillir mon téléphone et ma thermos ne rentre pas dans le porte-gobelet. De plus, elle n'a aucun compartiment caché et n'est pas parfaitement stable lorsque l'on conduit (qui conduit à des bons cafés renversés dans les virages), sa grande taille et sa forme imposant notamment de la poser côté passager et donc pas à portée de main du poste de conducteur.

**-> Contraintes liées à l'accessibilité, la sécurité routière, son emplacement et donc sa forme et sa visibilité, mais aussi sa fonction et donc sa stabilité et son adaptabilité**

<br>


# **ÉTAPE 3 :** Comprendre les dimensions spécifiques

Pour étudier les dimensions liées à l'objet et son confort d'utilisation, je les ai classé en trois groupes :

- les dimensions du tableau de bord : adaptabilité
- les dimensions du corps : être à portée de mains
- les dimensions des objets : stabilité et diversité

### Dimensions du tableau de bord

![Mesures tableau de bord](./images/projet-final/tableaudebord.jpg)

Afin d'adapter l'objet au tableau de bord des modèles T3 des vans volkswagen, j'ai réalisé **un relevé de toutes les mesures du tableau de bord de mon van** qui pourraient m'être utiles dans la conception et le dessin de la console. Par la suite, il sera question de le rendre plus universel en créant des paramètres qui pourraient le rendre utilisable sur d'autres modèles de vans.

![Mesures tableau de bord](./images/projet-final/mesures_tdb_corrige.jpg)

### Dimensions du corps

J'ai relevé deux mesures relatives au corps qui seront importantes dans la conception et le confort d'utilisation : **celle du bras**, qui permet de savoir jusqu'où les objets sont facilement à portée de main, et **la hauteur d'assise moyenne** afin que l'objet ne gêne pas dans la vision de la route.

Depuis la poste de conduite, pour schématiser la portée de notre bras, nous formons un triangle avec le tableau de bord.
Sachant qu'en moyenne, la distance entre l'épaule et le bout de nos doigts est de 65 à 70cm, celle entre nous et le tableau de bord de 40 cm, alors il est possible de calculer la distance maximum atteignable sur le tableau de bord afin d'avoir une idée générale de la longueur maximale de la console.

![Mesures triangle](./images/projet-final/mesures_triangle.jpg)

Pour ce qui est de la hauteur, lorsque l'on conduit, nous réglons le siège de façon à ce que les compteurs ne gêne pas notre vision, on peut alors prendre comme principe que la hauteur de la console ne doit pas dépasser celle des compteurs. Cette information doit également être prise en compte dans le dimensionnement des porte-gobelets par exemple, dont la profondeur doit permettre à la gourde ou thermos de ne pas venir gêner de manière trop importante notre vision.

### Dimensions des objets

Pour pouvoir dimensionner les objets qui prennent place dans la console, j'ai tout d'abord lister tout ceux que j'imaginais pouvoir être utile lorsque l'on conduit ou sur un tableau de bord, c'est-à-dire ceux auxquels on voudrait accéder rapidement en sortant du van.

**En roulant :**

- gobelets : thermos, bouteilles, verres, tasses
- de quoi grignoter : un fruit, des fruits secs, chewing-gum...
- un téléphone : rangé, en charge, en position GPS ou musique
- un GPS
- une carte routière ouverte et visible
- lunettes de soleil
- porte-feuille et papiers du véhicule ou monnaie
- stylo
- mouchoirs, médicaments

**En sortant :**

- clés
- masque
- gel hydroalcoolique
- écouteurs / casque
- affaires pour chien (laisse, friandises dans un sac ou compartiment emboitable, sachets poubelles)
- le porte-feuille et téléphone également

Parmi ces objets certains peuvent être dans un compartiment fermé / caché comme des bijoux, le porte-feuille, les lunettes ou encore les clés.

Je les ai ensuite dimensionnés en donnant des ordres de grandeurs / dimensions moyennes :

![Mesures objet](./images/projet-final/mesures_objet.jpg)

<br>


# **ÉTAPE 4 :** Esquisse, essai-erreurs, améliorations

Cette étape consiste à rentrer dans les premières esquisses et idées de la forme et le fonctionnemeent de notre objet ainsi que de tester différents prototypes (de la maquette au prototype fonctionnel en passant par les prototypes d'éléments clés comme des jonctions par exemple).

Pour ce qui est des esquisses, j'ai tout d'abord essayé de voir les façons dont je maintenais les objets jusqu'à maintenant dans ma voiture ou celles de mon entourage. J'ai relevé tout d'abord la console originale des T3 dont j'ai parlé plusieurs fois au cours de ma documentation (esquisse 1), puis la barre de tableau de bord avec le plateau (esquisse 2), qui permet d'accrocher des objets dessus mais qui nécessite alors d'avoir un adapteur pour chaque objet, le plateau lui n'offre aucune discrétion, aucune organisation ni un réel maintien pour les objets.

![Tdb exemple 0](./images/projet-final/tdb_ex_0.png)
![Tdb exemple 1](./images/projet-final/tdb_ex_1.png)

Ma première idée consiste en une console compacte avec des dimensions adaptées au champ de vision mais aussi adaptées afin d'atteindre tout depuis le poste de conduite. Sa hauteur permet de mettre en avant le téléphone, une carte, d'accrocher des objets à l'aide de crochets, de cacher des objets dans des compartiments fermés, mais aussi de tout disposer de façon pratique et utile.

![Tdb exemple 4](./images/projet-final/tdb_ex_4.jpg)

En discutant avec les autres, l'idée de venir créer une surface adhérente (type scratch ou aimants) sur le tableau de bord sur laquelle accrocher les objets directement (esquisse 3) viendrait répondre à la fonction de les tenir en place et de répondre à la diversité des objets, mais pose problème sur la mise en application car cela impliquerait d'avoir un surface adhérente sur chaque objet. Cela m'a conduit à l'idée de peut-être des modules qui auraient eux la surface adhérente et maintiendraient les objets (esquisse 4), permettant de switcher les modules selon l'utilisation et de rendre le concept adaptable à différentes personnes et véhicules. Le problème de cette idée reste dans le stockage des modules, la vie en van nécessite un gain de place maximum et devoir stocker les différents modules implique d'avoir la place pour.

![Tdb exemple 2](./images/projet-final/tdb_ex_2.jpg)
![Tdb exemple 3](./images/projet-final/tdb_ex_3.jpg)

<br>

## **IDÉE 1 :** console 'tout-en-un'

L'idée de la console 'tout-en-un' permettrait de manière compacte de maintenir différents objets et d'en cacher certain, elle reste pour l'instant la piste qui m'intéresse le plus pour l'aménagement de mon van d'un point de vue pratique car sa compacité implique un gain de place et d'efficacité.

Cette boîte serait maintenue pour les mouvements avants et arrières grâce à la bordure du tableau de bord, sa forme viendra épouser cette bordure pour caler la boîte. Pour augmenter l'adhérence notamment sur les mouvements latéraux, un système d'accroche doit encore être imaginé. Il y a la possibilité de créer une surface adhérente assez 'universelle' comme vue dans mes premières idées pour les objets mais cette fois pour la console entière, comme un scratch, des ventouses ou des aimants, qui rendraient la console adaptable à tous les véhicules et lui donnerait un maintien optimal.

D'après les mesures spécifiques listées à l'étape 3, j'obtiens un gabarit de cette taille :

![](./images/projet-final/mesures_boites.png)

Selon les objets listés et dimensionnés toujours à l'étape 3, j'ai esquissé deux idées de la conception de cette boite.

La première contient en surface de gauche à droite un porte-téléphone inclinable qui permet de poser un objet à plat ou incliné, un porte lunette, un porte-gobelet à plusieurs étages avec le système de sortie des pièces mentionnées précédemment, un petit bol pouvant contenir écouteurs, noix, ou autre et finalement un compartiment 'fourre-tout' avec un double fond permettant de cacher des objets. Dans la hauteur de la boîte se trouverait également une trappe donnant accès à un compartiment caché pour accueillir un porte-feuille par exemple, maintenue fermée par la pince du porte carte qui peut donc se tourner pour l'ouvrir, ainsi que des crochets (pour masque, laisse du chien etc).

![](./images/projet-final/idees_1_boite.png)

Pour la seconde esquisse, en surface on retrouve un bol encastré, le porte-gobelet/pièces, puis le porte-lunettes, un compartiment circulaire de rangement et la boîte à double-fond qui est cette fois beaucoup plus grande et remplace donc le compartiment caché avec trappe de la première esquisse.
Dans la hauteur de la console, un porte-téléphone qui permet de le voir beaucoup mieux depuis le volant, un porte carte (pince) et un crochet.  

![](./images/projet-final/idees_2_boite.png)

<br>

### **Test 1 :** fond du porte-gobelet avec sortie de pièces

J'ai tout d'abord voulu tester le système de porte-gobelet à plusieurs étages permettant d'y mettre plusieurs tailles de bouteilles/thermos qui intégrerait un porte-pièces tout au fond avec un accès à l'extérieur du module, qui grâce à la pente du tableau de bord et une petite trappe permettrait de récupérer toutes nos pièces rapidement.

![Modules](./images/projet-final/porte_gobelet_pieces.jpg)

Pour des économies de matière j'ai en premier temps testé le système de glissement des pièces que j'ai modélisé sur Fusion 360 et imprimé en 3D.

1/ Je trace une esquisse d'un cercle de 80mm de diamètre dans lequel je place une ligne à 50mm du bord qui divise mon cercle en deux parties et j'extrude la partie la plus grande du cercle sur 2mm.

![1 cercles](./images/projet-final/test-1/01_cercle.png)

2/ Sur la tranche de cet objet, je crée une nouvelle esquisse et je trace un rectangle de 15mm d'hauteur et de la largeur de la tranche que j'extrude sur 2mm également.

![](./images/projet-final/test-1/02_sketch.png)
![](./images/projet-final/test-1/03_extrudesketch.png)

3/ Sur la face du dessous du cercle extrudé, je trace deux cercles de 80 et 82mm. J'extrude la partie créer entre ces deux cercles sur 10mm pour créer une bordure.

![](./images/projet-final/test-1/04_sketchplan.png)
![](./images/projet-final/test-1/05_bordure.png)
![](./images/projet-final/test-1/06_extrudebordure.png)

4/ Sur une des faces latérales de la bordure verticale créée à l'étape 2 je trace un triangle de 15mm de hauteur avec un angle de 12° et je le déplace pour placer le coin de droite sur le coin inférieur gauche de la tranche comme sur l'image ci-dessous.

![](./images/projet-final/test-1/08_sketchtriangle.png)

5/ J'extrude le rectangle créé à l'étape 2 jusqu'au bout du triangle.

![](./images/projet-final/test-1/07_extrudehauteur.png)

6/ J'extrude le triangle jusqu'à l'autre côté du rectangle créé.

![](./images/projet-final/test-1/09_triangle.png)

7/ Pour créer le fond je fais une nouvelle esquisse sur le triangle et je rajoute une bande de 2mm tout le long avec l'outil ligne.

![](./images/projet-final/test-1/10_fond.png)

8/ Pour créer la bordure supérieure je crée une nouvelle esquisse sur la tranche circulaire du haut du corps principal et je trace deux cercles, de 80 et 90mm et j'extrude la forme créée sur 2mm.

![](./images/projet-final/test-1/11_cercles.png)
![](./images/projet-final/test-1/12_extrudebord.png)

J'exporte en .stl et je l'ouvre sur PrusaSlicer pour l'imprimer, avec l'outil Place on Face (F) je le mets à plat sur le fond qui permet la descente des pièces.

![](./images/projet-final/test-1/13_prusaslicer.png)

Les paramètres d'impression sont les suivants, j'ai activé les supports automatiques et j'ai rajouté une jupe, j'ai également mis le remplissage sur 0 sachant que les parois sont d'une épaisseur de 2mm comme le fil. J'obtiens donc ceci, j'ai exporté le g-code et lancé l'impression de 5h01, que j'ai passé à une vitesse de 150% vers la moitié de l'impression.

![](./images/projet-final/test-1/13_reglages2.png)
![](./images/projet-final/test-1/13_reglages.png)
![](./images/projet-final/test-1/13_jupe.png)
![](./images/projet-final/test-1/13_objet.jpg)

Après environ 4h30 d'impression, voilà le résultat final :

![](./images/projet-final/test-1/14_objet_final.jpg)

Grâce à ce test, j'ai pu constater que la taille de mon plus petit porte-gobelet est beaucoup trop grande, je l'ai dimensionné à 80mm et par exemple avec une canette, elle se balade beaucoup trop dedans et n'est pas maintenue. Je dois donc plutôt passer à 60mm pour la prochaine fois.
En revanche, pour les pièces, le système fonctionne très bien et les pièces tombent directement quand on ouvre la trape et ne restent pas coincées.

<br>

### **Test 2 :** corps principal de la console en bois

Cette semaine j'ai décidé de fabriquer un premier prototype de la console pour tester :

- son adaptabilité à la forme du tableau de bord
- ses dimensions : hauteur adaptée au champ de vision ? largeur accessible depuis le volant ?
- transportable ?

![](./images/projet-final/test-2/axo_boite_g.jpg)

    Matériaux : plaques de bois récupérées (couvercles de palettes) de 10mm d'épaisseur + scie sauteuse


**Prototype 1**

J'ai tout d'abord réalisé un prototype de partie latérale pour tester si la console allait épouser correctement le tableau de bord en me basant sur le relevé effectué et en retirant les 10mm d'épaisseur en haut et sur le côté avant.

![](./images/projet-final/test-2/proto_1_boite_patron.jpg)

1/ Je trace le patron sur la planche de bois avec une équerre et un rapporteur

2/ Je coupe à la scie sauteuse

3/ Je lime pour lisser les découpes

![4 photos etapes prototype 1](./images/projet-final/test-2/proto_1_boite_test.jpg)

Ce premier test m'a permis de voir que mon relevé n'était pas exact et la face latérale ne se pose pas correctement sur le tableau de bord, elle est bancale car elle ne touche pas à l'arrière à cause de l'arrondi léger de la surface.

J'ai donc marqué l'endroit jusqu'où le bois touchait le tableau de bord et j'ai mesuré l'écart au coin arrière pour pouvoir modifier mon patron.

![photo prototype 1 console](./images/projet-final/test-2/proto_1_boite_final.jpg)

**Prototype 2**

Mon nouveau patron ressemble maintenant à ça :

![](./images/projet-final/test-2/proto_2_boite_patron.jpg)

Je trace à nouveau mon patron pour la nouvelle face latérale que je découpe. Cette fois-ci, elle épouse bien le tableau de bord et assez de points d'appui pour bien tenir sans aucune bancalité mais l'avant est trop long car avec l'épaisseur du bois la console va beacoup dépasser. J'ai donc retiré 5mm.

![3 photos prototype 2](./images/projet-final/test-2/proto_2_boite_test.jpg)

1/ Maintenant que la face latérale fonctionne, j'en découpe une deuxième en traçant le contour de cette première sur le bois.

2/ Puis je trace et coupe tout d'abord le plateau supérieur premièrement (en suivant mes esquisses de patron) et la face avant ensuite, pour laquelle j'utilise la longueur du plateau comme référence et les esquisses pour la hauteur.

***Conseil*** : faites les pièces une par une, c'est-à-dire tracez le patron d'une pièce, puis coupez là, puis tracez la seconde etc. sinon vos pièces n'auront pas la bonne dimension suite à l'épaisseur de la coupe !

3/ Je cloue le tout à chaque coin.

![4 photos etapes prototype 2](./images/projet-final/test-2/proto_2_boite_assemblage.jpg)

Voilà le premier prototype de la console complète terminé. Grâce à lui je peux voir que la largeur adaptée mais que je ne dois surtout pas la faire plus large sous peine de ne plus atteindre l'extrémité en étant au volant. La hauteur de la console également est adaptée mais ne doit pas être plus grande : la console ne dépasse pas de façon gênante devant le pare-brise.

![3 photos prototype 2 final](./images/projet-final/test-2/proto_2_boite_final.jpg)

<br>

### **Test 3 :** découpe des compartiments

Pour rendre cette console adaptable et universelle, je pense proposer la modélisation de plusieurs compartiments paramétriques permettant aux gens de choisir les modules qu'ils souhaitent placer sur leur console.

Pour ma part, j'ai tout d'abord testé les deux organisations que j'avais esquissé sur papier pour mieux visualiser le tout et les dimensions.

(/!\ prendre en photo les deux feuilles)

La seconde me paraît plus adaptée à mes besoins et c'est celle que j'ai choisi de tester dans mon prototype de console.

Pour cela, j'ai donc modélisé en dessin vectoriel les différents compartiments à découper dans le plateau supérieur en faisant une esquisse sur Fusion 360.

![](./images/projet-final/test-2/module1_console01.jpg)

J'exporte mon esquisse en .dwg, puis ouvert sur Autocad et exporté en dxf pour pouvoir l'ouvrir sur inksape. Sur inkscape je règle mon plan de travail à 500x250mm, l'épaisseur de trait à 0.2mm et j'exporte sous forme de fichier svg.

![](./images/projet-final/test-2/module1_console03_autocad.png)

J'ai réalisé les découpes à la Shaper avec une fraise de 6mm, en découpes intérieures à la ligne et sur une profondeur de 10mm sur le plateau supérieur (que j'ai décroché de la console pour qu'il soit à plat).

J'ai obtenu ce résultat :

![](./images/projet-final/test-3/decoupe-shaper-console.jpg)

Au niveau de la matière restante après découpe, les chutes ne sont pas très nombreuses et peuvent être réutilisées pour d'autres projets grâce à leur forme.
Pour ce prototype, les angles à la Shaper ne sont pas très propres ou nets car j'ai rencontré quelques problèmes de paramétrage avec le 'z touch' de la machine qui ont fait que j'ai du m'y reprendre à plusieurs reprises. Pour effectuer le paramétrage du 'z touch' correctement il faut placer la machine sur une surface parfaitement plane, si votre planche de bois est quelque peu courbée ou que la surface n'est pas totalement lisse, il ne se fera pas correctement.

<br>

### **Test 4 :** les aimants

Afin de faire tenir la console au tableau de bord tout en la rendant transportable, j'ai opté pour des aimants qui se fixeront directement sur le tableau de bord qui est en métal.

Après avoir testé des aimants de 10, 20 et 30kg de force, le choix s'est porté sur ceux de 20 qui permettent de bien tenir la console mais qui ne sont pas trop durs pour pouvoir la retirer.

Pour les fixer au fond de la console, j'ai choisi des aimants percés au centre qui permettent des les visser. Je les ai fixé à l'aide d'une petite équerre de chaise, en utilisant une vis de 9mm maximum afin d'éviter qu'elle ressorte de l'autre côté pour fixer l'équerre à la face latérale, et une vis à bout plat maintenue avec un écrou pour la fixer à l'aimant,

J'ai placé un aimant à peu près au centre de chaque face latérale. Il faut bien faire attention à aligner la partie du bas de l'aimant avec le bord de la face latérale de la console sous peine d'avoir une console bancale une fois posée sur le tableau de bord.

![](./images/projet-final/test-5/console_aimants.jpg)

Je dois encore rajouter une surface protectrice fine devant les aimants pour protéger le tableau de bord des rayures.

<br>

### **Test 5 :** réalisation des différents compartiments

La console comporte 5 compartiments en surface et le porte-téléphone à l'avant. J'ai choisi d'utiliser deux techniques différentes adaptées à la forme des objets. Les compartiments de formes arrondies seront imprimés en 3D et ceux en forme de boîtes découper à la lasercut.

![](./images/projet-final/boite_compartiments.jpg)

#### **1/ Le bol**

Le premier compartiment que j'ai souhaité réalisé est le petit bol en haut à gauche. Du fait de sa forme courbe, j'ai choisi de l'imprimer en 3D. Pour cela, je l'ai donc modélisé sur Fusion 360.

Je trace un cercle de 70mm de diamètre, avec l'outil *sphère* je fais une sphère de 70mm de dimaètre, puis je l'évide avec l'outil *shell* vers l'intérieur sur 2mm et je coupe l'objet obtenu en 2 parties avec l'outil *Split body* en sélectionnant le plan de coupe horizontal au milieu de l'objet et je supprime (ou masque) la partie du haut.

![](./images/projet-final/test-4/console_bol_01.jpg)

Ensuite, pour pouvoir par la suite le coller sur la face inférieure de mon plateau, je crée un débord en faisant une nouvelle esquisse sur le bord supérieur dans laquelle je trace un cercle de 80mm de diamètre que j'extrude de 1mm vers le bas. Avec l'outil *Hole* je perce la surface obtenue sur 1mm avec un diamètre de 68mm.

![](./images/projet-final/test-4/console_bol_02.jpg)

Finalement, pour l'impression, je choisis les paramètres suivants :

![](./images/projet-final/test-4/console_bol_03.jpg)

Je place le bol à l'endroit pour éviter des supports à l'intérieur du bol qui seraient plus difficiles à retirer et risqueraient d'abîmer la face visible de l'objet. L'impression s'est très bien passée et les supports se sont retirés facilement.

Pour le fixer au prototype de la console, j'ai décidé d'utiliser du scotch double face pour l'instant afin de pouvoir le fixer sur le prototype final pour éviter de l'imprimer une seconde fois -et donc économiser de la matière- alors qu'il n'a aucun défaut. J'ai tracé le contour de mon débord sur du double-face que j'ai coupé et collé par le dessous du plateau. Le résultat fonctionne très bien.

![](./images/projet-final/test-4/console_bol_final.jpg)

<br>

#### **2/ Le porte-stylo ou cylindre**

Comme pour le bol, j'ai décidé de réaliser le compartiment cylindrique en haut à droite à l'impression 3D.

Sur Fusion 360 j'ai tout d'abord, dans une nouvelle esquisse sur le plan horizontal, tracé deux cercles de 40 et 43mm, j'ai extrudé la partie entre les deux de 54mm vers le bas.
De la même façon que le bol, j'ai créé également un débord pour pouvoir l'accrocher au plateau, en faisant une nouvelle esquisse sur la tranche inférieure de l'objet créé et en traçant un cercle de 60mm et un de 43, j'extrude la partie entre les deux de 3mm vers le bas.
Finalement, pour créer un fond, je fais une esquisse sur le cercle du fond, je trace un cercle de 40 et je l'extrude de 5mm vers le haut.

![](./images/projet-final/console_tube_01.jpg)

J'ai choisi les mêmes paramètres que pour l'impression du bol. L'impression a duré 3h et s'est très bien passée. Je l'ai attaché avec du double-face comme pour le bol.

![](./images/projet-final/test-5/PS_impression01.jpg)

<br>

#### **3/ Les boîtes**

Pour réaliser les boîtes à la lasercut, voici les patrons que je dois suivre :

![](./images/projet-final/boite_patrons_comp.jpg)

*Note : e(parois) représente l'épaisseur du carton bois et e(plateau) celle du plateau en bois qui est de 10mm pour ma part.*

    Matériaux : afin d'être découpable à la laser tout en ayant une certaine solidité, carton gris de 2mm

J'ai donc dessiné en vectoriel mes différents patrons sur Autocad et exporté sur Inkscape pour obtenir un fichier en .svg afin de l'envoyer vers le logiciel de la découpe laser.

![](./images/projet-final/test-4/console_decoupe_patron.png)

J'ai utilisé l'Epilog pour effectuer ma découpe dans le carton gris, après avoir effectué deux tests de réglages, j'ai choisi pour l'ensemble des tracés une vitesse de 15% et une puissance de 70% en mode *découpe*.

Le plus gros compartiment était trop profond et dépassé de la console, j'ai donc recoupé 20mm sur chaque faces verticales au cutter. J'ai ensuite collé les éléments ensemble avec de la colle à bois pour former mes deux compartiments et le porte-téléphone, que j'ai accroché à la console sur la tranche de la face supérieure avec de la colle à bois également (je voulais initialement les clouer mais l'emplacement rendait le clou inaccessible avec le marteau) ainsi que des serre-joints le temps du séchage. Une fois sec, le maintien est très correct.

J'ai ensuite peint l'intérieur et les tranches des compartiments en noir, avec du scotch de peindre pour protéger la console, pour rendre les compartiments plus homogènes avec ceux imprimés en 3D en PLA noir.  

![](./images/projet-final/test-5/console_compartiments01.jpg)

Le porte-téléphone est collé sur la face avant côté conducteur, je dois encore trouver un moyen de pouvoir accueillir un téléphone en position couchée comme debout sans qu'il se balade de droite à gauche dans cette seconde position, et rajouter un trou pour pouvoir le charger en même temps.

![](./images/projet-final/test-5/console_compartiments03.jpg)

Finalement, pour le double fond du plus gros compartiment, j'ai récupéré des chutes de la découpe de la console en bois pour faire deux pieds de 20mm de haut que j'ai collé à chaque extrémité du fond du compartiment. J'ai coupé un rectangle de 15mm de large et 60mm de long dans du tissus que j'ai peint en noir et agrafé en le pliant en deux sur le dessous du rectangle restant pour créer une languette permettant de soulever le double fond.

![](./images/projet-final/test-5/console_compartiments02.jpg)

Voici mon premier prototype de console terminé, malgré qu'il manque encore le porte-gobelet que je n'ai pas pu réimprimer pour l'instant, mais j'ai pu tester celui de mon module qui fonctionne également sur la console, ainsi que le porte-carte et les crochets.

![](./images/projet-final/test-5/console_prototypefin1.jpg)

<br>

## **IDÉE 2 :** modules

La seconde idée est donc celle des modules adhérents.

Afin de rendre l'objet universel et adaptable aux besoins et envies des gens, je pensais créer un ou deux modules en bois extérieur sans couvercle dans lesquels viendraient s'emboîter différents 'compartiments' imprimés en 3D, permettant alors aux gens de créer leurs propres compartiments ou de choisir ceux qu'ils veulent en partant toujours d''uniquement une ou deux même bases en bois réalisables avec un grand nombre d'outils : à la Shaper, CNC, comme à la scie sauteuse.

Les deux modules seraient un plutôt cubique pour le porte-gobelet, un fourre-tout, un porte lunettes etc. Et le second rectangulaire pour pouvoir créer un double fond pour portefeuille, un fourre-tout, ranger son téléphone etc.

Un aimant en dessous et un sur la face arrière permet de les placer à différents endroits : sur le tableau de bord, sur une table, contre la tôle du van dehors...

![](./images/projet-final/module_deuxtypes.jpg)

<br>

### **Test 1 :** module extérieur

Pour mon premier prototype de module, j'ai décidé de réaliser la boîte en bois à la scie sauteuse.

    Matériaux : plaques de bois récupérées (couvercles de palettes) de 10mm d'épaisseur + isorel récupéré d'un meuble (fond)

Je me suis basée sur ce patron (attention ces esquisses prennent donc une épaisseur de 10mm en compte) :

![](./images/projet-final/test-2/proto_1_module_patron.jpg)

1/ Je trace le patron sur le bois à l'aide d'une équerre, un réglet et un rapporteur d'angle.

2/ Je coupe à la scie sauteuse à l'extérieur du trait.

![photo patron coupe](./images/projet-final/test-2/proto_1_module_decoupe.jpg)

3/ Je cloue les 4 côtés ensemble à chaques coins avec des clous assez fins pour éviter d'éclater le bois.

4/ Pour le fond, j'utilise de l'isorel, du bois fin utilisé pour les fonds de meuble. Je le trace en plaçant mon module maintenat cloué sur la plaque d'isorel et je trace le contour que je coupe à même le trait.

![photo fond](./images/projet-final/test-2/proto_1_module_fond.jpg)

Dans mon premier prototype, j'ai ajouté le trou pour les pièces, mais une fois terminé en prenant l'objet dans ma main je me suis rendu compte que ce système était adapté à une console imposante (de l'idée 1) mais était inutile dans un module si petit et léger sachant que pour récupérer des pièces il suffit de retourner le module, ce système rendant l'objet techniquement beaucoup plus compliqué et utilisant donc plus de matières inutilement.

De plus, en clouant au marteau, j'ai abîmé le bois qui est peu résistant en tapant trop fort.

![4 photos prototype 1 module fini](./images/projet-final/test-2/proto_1_module_fini.jpg)

Par conséquent, j'ai fabriqué un deuxième prototype sans ce trou, juste une boîte pouvant être adaptée pour emboîter différents modules de plastique. J'en ai profité pour clouer plus doucement en posant toujours la boîte de façon à clouer à la verticale depuis le dessus.

Ce prototype s'adapte parfaitement au tableau de bord du van !

![photo prototype 2 module final](./images/projet-final/test-2/proto_2_module_final.jpg)

<br>

### **Test 2 :** les aimants

Comme pour la console, j'ai opté pour des aimants de 20kg afin de maintenir l'objet sur le tableau de bord, mais à la différence de la console, également en l'accrochant à des surfaces verticales.

Pour cela, j'ai fixé l'aimant directement sur la face arrière à l'aide d'une vis de 12mm (pour qu'elle ne ressorte pas de l'autre côté, l'épaisseur de l'aimant permettant une vis plus grande). Il tient alors très bien à la verticale.

![](./images/projet-final/test-5/module_aimants.jpg)

Je dois encore trouver un moyen de fixer l'aimant au fond du module. Contrairement à la console, le module comporte un fond (pour qu'à vide, on puisse toujours l'utiliser), si je fixe l'aimant directement à même le fond (sketch n°2) comme je l'ai fait pour l'aimant de la face arrière, le module sera bancal et décollé du tableau de bord. J'ai réalisé, en testant l'aimant, qu'il arrivait à aimanter bien au travers de l'isorel qui est assez fin, je l'ai donc fixé comme sur le sketch n°3 ci dessous en enfonçant la tête de vis dans l'isorel. Une meilleure solution pourrait être de l'encastrer dans le fond directement (n°1).

![](./images/projet-final/test-5/module_aimantsfond.jpg)

Je dois aussi rajouter une surface protectrice fine devant l'aimant pour protéger la surface sur lequel je vais l'accrocher et éviter les rayures.

<br>

### **Test 3 :** compartiments emboîtables en impression 3D

#### **1/ Premier test de porte-gobelet**

Pour mon premier test de compartiment, j'ai décidé de modéliser le porte-gobelet à plusieurs étages, dont j'ai changé les dimensions suite à mon tout premier test qui était bien trop grand.

Je commence par le module en bois afin de pouvoir l'utiliser comme référence.

1/ Je crée un paramètre 'epaisseurbois' afin de pouvoir varier le module selon le bois choisi.

2/ Je crée une esquisse comme ci-dessous avec un carré de 100x100mm et un de 100+(epaisseurbois*2) de côté, j'extrude la forme obtenue sur 140mm.

3/ Sur une des faces latérales, je crée une nouvelle esquisse dans laquelle je trace une ligne partant du coin du bas et avec un angle de 18° jusqu'à l'arrête opposée. Je l'extrude de 120mm en opération *Cut*.

![](./images/projet-final/test-3/module1_modelisation.jpg)

Mon module est terminé.

J'ai donc modélisé le premier compartiment emboîtable et que j'ai imprimé en 3D :

1/ Je crée une nouvelle esquisse sur la bordure supérieure du module et je trace un carré de 100x100mm que j'extrude vers le bas de 2mm.

2/ Sur ce carré, je crée une nouvelle esquisse dans laquelle je trace deux cercles, de 100 et 98mm de diamètre. J'extrude le cercle intérieur de plus de 2mm en opération *Cut* et j'extrude le cercle extérieur de -90mm en opération *New Body*.

![](./images/projet-final/test-3/pg1_modelisation01.jpg)

3/ Sur le cercLe du bas de ce nouveau corps, je crée une nouvelle esquisse pour tracer deux cercles de 98 et 80mm. J'extrude la forme obtenue de 2mm vers le bas.

4/ Sur la partie supérieure de la forme, je fais à nouveau une esquisse de deux cercles de 80 et 82mm et j'extrude la forme crée entre ces deux cercles de 10mm vers le bas.

![](./images/projet-final/test-3/pg1_modelisation02.jpg)

5/ Je répète l'opération 3 et 4 pour le porte-gobelet de 60mm.

6/ Je crée une esquisse sur le cercle du bas du corps créé pour le porte-gobelet de 60mm comme à l'étape 3 mais je ne trace qu'un cercle de 60mm que j'extrude de 2mm vers le haut pour créer le fond.

5/ Avec l'outil *Combine*, je fusionne les différents corps obtenus en un seul pour former l'objet porte-gobelet.

![](./images/projet-final/test-3/pg1_modelisation03.jpg)

6/ Je crée une nouvelle esquisse dans le fond du porte-gobelet pour ajouter un support permettant au compartiment de reposer sur le fond du module et acceuillir des pièces de monnaies. Pour cela je trace deux cercles de 40 et 42mm et j'extrude la forme obtenue vers le bas de *- 30mm - epaisseurbois* afin que si l'on change l'épaisseur du bois ce support pose toujours sur le fond du module.

7/ Je crée un nouveau fond en répétant l'étape 6 avec un cercle de 40mm cette fois-ci, extrudé de 2mm vers le haut.

L'impression a duré environ 9h30 et j'ai rencontré plusieurs soucis. Tout d'abord, le réglage de l'espacement du quadrillage de mes supports était trop généreux (10mm) et a empêché la bonne imperssion de la première couche de chaque porte-à-faux. Les connexions entre les pans verticaux et horizontaux sont également très fragiles. De plus, une fois imprimé, je me suis rendue compte d'un élément important que j'ai totalement omis : le fond de mon module en bois est en biais pour suivre le tableau de bord, hors j'ai modélisé le fond du porte-gobelet à l'horizontal et il ne rentre donc pas dans le module.

![](./images/projet-final/test-3/impression-1-pg.jpg)

#### **2/ Deuxième test de porte-gobelet**

J'ai essayé à nouveau de réaliser le porte-gobelet emboîtable dans le module en modifiant la modélisation sur Fusion 360.

1/ Je repasse, sur ma timeline en bas de l'écran, à ce qui correspond à l'étape 5 de mon premier test : une fois que j'ai combiné mes volumes et avant de recréer un étage de 40mm de diamètre.

2/ Je remonte le débord afin de faire poser le compartiment sur l'épaisseur des bords du module. Pour cela, je commence par extruder la face supérieure du débord de 2mm vers le haut, puis je crée une esquisse sur le fond de ce débord et je trace un carré sur le contour déjà existant que j'extrude ensuite de -2mm vers le haut en opération *Cut*. Finalement j'extrude la tranche supérieure de mon cylindre de 2mm vers le haut pour rejoindre le débord.

3/ J'élargis le débord en extrudant les tranches latérales des quatre côtés de 8mm.

![](./images/projet-final/test-4/pg2_modelisation01.jpg)

Cette fois, j'ai décidé de l'imprimer à l'envers, avec des supports à l'intérieur mais qui permettent une meilleure stabilité et moins de porte-à-faux, et donc moins de chances que l'impression ait des soucis. J'ai suivi ces paramètres d'impression :

![](./images/projet-final/test-4/pg2_impression01.jpg)

L'impression a duré 9h30 et j'ai rencontre à nouveau des problèmes d'impression au niveau des plans horizontaux malgré que j'ai rétrécit l'écart des supports. La création d'un 'raft' a également abîmé la surface plane du dessus en l'enlevant car il était complètement soudé avec l'objet.

Le second problème est que le compartiment est toujours trop grand dans sa hauteur et dépasse du module. Je me suis rendu compte que le problème venait de ma modélisation du module en bois où je me suis trompée dans les dimensions en mettant 14cm au lieu de 13 sur la plus grande hauteur, j'ai donc modélisé le porte-gobelet en conséquence.

![](./images/projet-final/test-4/pg2_impression02.jpg)

Pour tester si le concept fonctionne malgré tout avant de relancer une impression de 9h, j'ai coupé la partie la plus haute et j'ai retiré un anneau de 10mm, ce n'était toujours pas suffisant alors j'ai retiré 5mm en plus. Maintenant le compartiment s'emboîte très bien et supporte même une gourde remplie en étant aimanté.

![](./images/projet-final/test-4/pg2_impression03.jpg)

<br>

### **Test 4 :** assemblage à queues droites

L'assemblage à queue droite est un moyen de joindre deux éléments en bois sans vis ou clous.

![](./images/projet-final/test-final/boxjoint_principe.jpg)

Pour cela, je les réalise à la Shaper, mais pas dans son utilisation habituelle avec ma planche à plat, car la forme de la fraise empêcherait d'avoir des angles droits dans les coins. En effet, il faut maintenir le bois avec la tranche vers le haut, en le fixant au côté de la table, cela permet d'avoir des découpes carrées.

![](./images/projet-final/test-final/decoupe_boxjoints_explic.jpg)

Pour mon premier test, j'ai tout d'abord découpé les différents éléments à la scie puis j'ai testé la découpe des joints mais cela n'a pas été concluant car avec le patron que j'ai utilisé pour leur découpe, le résultat n'était pas propre du tout.

![](./images/projet-final/test-final/test-1-boxjoints.jpg)

<br>

### Pré-jury : retours

Lors du pré-jury de décembre, j'ai présenté tous mes prototypes et mes deux idées : la console et le module. Les entretiens avec les différents jurys m'ont beaucoup éclairé sur comment continuer, et pour résumer en quelques mots, la grande majorité m'a conseillé d'approfondir la piste du module qui est plus facile à rendre universel en proposant par exemple des éléments emboîtables fabriqués selon différentes techniques comme le thermoformage par exemple. Il m'a également été conseillé de fabriquer plusieurs de ces modules afin de montrer comment il est possible de les accrocher ensemble pour former une console à part entière. Finalement, l'idée de trouver une certaine identité graphique pour l'objet est ressortie.

# **ÉTAPE 5 :** Adaptation, diffusion, édition

À la suite de ces pré-jurys, j'ai donc décidé de me concentrer sur la finalisation et l'approfondissement de l'idée n°2 : celle du module.

### Concept

L'idée est donc d'avoir un module simple en bois, dans lequel viendront se glisser différents éléments détournables à l'infini selon les besoins et envies et permettant de ranger ce que l'on veut, cela grâce à divers modes de fabrication : impression 3D, thermoformeuse pour l'emprunte d'un objet qui nous est propre, découpe laser, etc. Les modules, à l'aide d'aimants, peuvent se fixer partout dans le van : sur le tableau de bord, sur la carrosserie à l'extérieur, près du lit... et peuvent également s'aimanter entre eux pour former une console complète sur le tableau de bord !

![](./images/projet-final/test-final/modules_fabrication.jpg)

<br>

## **IDÉE FINALE**

## **Le module**

J'ai choisi de réaliser mon module dans des planches de sapin brut de 12mm d'épaisseur, avec un coût moyen de 30€/m2. Le choix de planches de 120mm de largeur permet **d'éviter les chutes** car chaque élément du module mesure 120mm de largeur, je peux donc découper chaque élément directement dans cette planche en reportant les longueurs en ne perdant aucune quantité de bois.

Les patrons que j'ai réalisé pour les différents éléments de la boîte sont ceux ci-dessous :

![](./images/projet-final/test-final/patron_boite.jpg)

Ce patron permet de réaliser par la suite des assemblages à découper dans l'élément comme ceux à queue droite, queue d'aronde, etc.

![](./images/projet-final/test-final/patron_boite_2.jpg)

Ce patron permet lui de réaliser des assemblages comme ceux à tourillons, ou par exemple pour visser ou clouer les éléments ensemble.

Le choix du bois permet d'avoir un module solide, fabriqué rapidement et avec peu de chutes de bois, mais il est également possible de le réaliser en plastique à l'impression 3D.

J'ai modélisé le module avec différents paramètres : l'épaisseur du bois, le côté de la boîte, sa hauteur, le diamètre et l'épaisseur de l'aimant et de la rondelle en métal.

![](./images/projet-final/test-final/parametres.png)

Le fichier .stl du module (et de ses éléments emboîtables) est disponible sur [ce lien](https://a360.co/3IG1KrN).

<iframe src="https://myulb184.autodesk360.com/shares/public/SH9285eQTcf875d3c5390f90afbf8fbff472?mode=embed" width="800" height="600" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

<br>

### Jonctions : assemblage à queue droite

Pour joindre les différents éléments, je vais réaliser un module en utilisant des assemblages à queue droite qui permettent une bonne jonction tout en donnant un bon aspect visuel à la boîte.

Après mon premier test non concluant de découpe de cet assemblage décrit dans la partie précédente, j'ai découvert après quelques recherches que Shaper propose une extension gratuite Box Joints qui permet justement de réaliser ce type d'assemblage sans avoir à faire son propre patron de découpe ou des calculs, ce qui permet d'éviter les erreurs et d'avoir un résultat propre. Ainsi, pour mon second test, j'ai tout d'abord coupé 3 fois les éléments du patron du module à la scie, puis j'ai utilisé l'extension *box joints* de la Shaper.

Pour cette extension, j'ai tout d'abord coupé un rectangle de 200 par 80mm dans un plan de travail en bois à la Shaper, que j'ai ensuite disposé de façon à ce que le rectangle découpé soit dans le vide avec une de ses longueurs le long du bord de la table. J'accroche une des faces de mon module à la verticale contre la table, tranche vers le haut que je fixe avec des serre-joints. Ensuite, je scanne mon plan de travail, je crée une nouvelle grille qui correspond à la tranche à découper et j'utilise l'extension : j'ai utilisé une fraise de 6mm de diamètre avec un premier passage à 6mm de profondeur puis un à 12.

J'ai suivi [ce tutoriel de ShaperTools](https://support.shapertools.com/hc/en-us/articles/360034065933-Box-Joint-Basic) pour réaliser mes assemblages.

*/!\ Il faut veiller à bien fixer l'élément à découper pour qu'il ne bouge pas lors de la découpe !*

Pour mon premier essai de l'extension, j'ai fait l'erreur de ne pas refaire une nouvelle grille pour chaque élément, et avec l'imprécision du placement de l'élément il ne se retrouve pas exactement au même endroit que la précédente et j'ai donc eu un décalage dans mon assemblage.

![](./images/projet-final/test-final/test-2-extension-boxjoints.jpg)

Pour mon second essai, j'ai donc scanné mon plan de travail avec les shapertapes et créé une nouvelle grille pour chaque découpe des joints. Sur les tranches de 130mm, j'ai choisi de mettre 5 queues d'assemblage et 4 pour celles de 95mm. Dans les réglages de l'extension, je rentre  l'épaisseur de mon bois de 12mm, sa largeur (130 ou 95), le nombre de queues et l'écart entre chaque, que j'ai finalement mis sur 0,08 pour mes planches en sapin. J'ai ensuite limé et poncé pour finaliser la découpe.

![](./images/projet-final/test-final/module-assemble.jpg)

### Jonctions : assemblage par tourillons

La seconde façon que je propose après mes différents tests pour assembler les modules est un assemblage réalisable sans Shaper : l'assemblage par tourillons.

![](./images/projet-final/test-final/tourillons_principe.jpg)

Pour les réaliser, j'ai choisi des tourillons de 6mm de diamètre, mon bois étant d'une épaisseur de 12mm.
À l'aide d'un guide de perçage spécifique pour ces assemblages, je perce chaque côté des éléments du module de 2 trous avec un forêt de 6mm. Il faut tout d'abord percer ceux dans la tranche, puis ceux dans la surface du bois grâce à l'outil qui permet une correspondance parfaite entre les deux éléments s'emboîtant ensemble.

*/!\ Pour les côtés où le tourillon vient se loger dans l'épaisseur du bois, il faut bien veiller à ne pas percer jusqu'au bout, dans mon cas j'ai créé un repère sur mon forêt pour ne percer qu'à 8mm maximum, il faut également percer le trou opposé à la profondeur correspondant au reste du tourillon + 1 mm de jeu pour un emboîtement parfait, par conséquent ici pour des tourillons de 30mm j'ai percé l'opposé de 23mm.*

![](./images/projet-final/test-final/tourillons-module.jpg)

Cette méthode étant plus rapide et simple à réaliser, j'ai fabriqué 4 exemplaires de modules pour pouvoir les emboîter ensemble et former une console.

### Aimants et logo

Pour les aimants, j'ai utilisé ceux présentés précédemment d'une force de 20kg, d'un diamètre de 28mm et d'une épaisseur de 5mm. L'idée est donc de les encastrer dans le bois afin qu'ils ne dépassent pas. Pour chaque aimant, il faut également une plaque en métal sur le module à l'opposé, pour cela j'ai opté pour des simples rondelles de 22mm de diamètre et 2mm d'épaisseur. Pour la face du dessous, j'ai choisi des aimants en bande autoadhésive dont l'aimantation suffit amplement pour ne pas qu'ils glissent sur le tableau de bord.

Le placement des aimants permet de les aimanter l'un devant l'autre ou l'un à côté de l'autre, ainsi que contre une paroi verticale en métal ou sur le tableau de bord.

Voici le placement des aimants :

![](./images/projet-final/test-final/aimants_placement.jpg)

#### Rondelles

![](./images/projet-final/test-final/etape_2_rondelles.jpg)

Tout d'abord, pour encastrer les rondelles, je découpe à la Shaper un cercle de 22,5mm et sur une profondeur de 2,2mm pour le côté (sur sa face extérieur, en son centre) et de 10mm pour la face avant, que je découpe uniquement pour ce cas précis du côté intérieur afin de cacher la rondelle et à 50mm du bas de la plaque pour qu'ils correspondent à l'aimant d'en face lorsque posé sur la pente du tableau de bord, celle-ci sert uniquement à maintenir les modules ensemble sur le tableau de bord donc l'aimantation ne doit pas être extrêmement forte. Je fixe la première à l'aide d'une vis de 8mm pour ne pas qu'elles dépassent de l'autre côté de la plaque de bois, et à l'aide de colle à bois pour la seconde qui est déjà bien maintenue par la profondeur d'enfoncement dans le bois (pas de vis car il ne reste que 2mm de bois).

![](./images/projet-final/test-final/decoupe_rondelles.jpg)

#### Aimants

![](./images/projet-final/test-final/etape_3_aimants.jpg)

Pour les encastrer dans le bois, je découpe à nouveau à la Shaper dans l'autre côté et la face arrière (côté extérieur pour les deux et en leur centre) la forme de mon aimant sur une profondeur correspondant à l'épaisseur de l'aimant avec un petit jeu pour s'assurer qu'il s'emboîte parfaitement. Ici, cela correspond à un cercle de 29mm et une découpe de 5,5mm de profondeur, puis je les visse comme pour les rondelles avec des vis de 8mm.

![](./images/projet-final/test-final/decoupe_aimants.jpg)

Par dessus les aimants, je conseillerai de rajouter une légère feuille de plastique, de papier ou même de tissus (toute matière fine n'empêchant pas l'aimantation) afin de protéger les surfaces sur lesquelles on va aimanter le module.

Pour l'aimant du dessous, j'utilise une bande de 15mm x 1,8mm que je découpe aux ciseaux pour la coller de part et d'autre du fond en isorel. Malheureusement mon choix d'aimant n'est pas du tout fort et l'aimantation est très faible même si elle permet malgré tout de maintenir les modules sur le tableau de bord. Il faudrait veiller à prendre un aimant en bande fort ou bien coller des petits aimants normaux à chaque coin.

![](./images/projet-final/test-final/aimant-dessous.jpg)

#### Logo

Pour l'identité graphique expliquée en dernière partie de cette page, j'ai voulu essayer de graver un van T3 sur la face avant du module. Le sapin n'étant pas recommandé pour les gravures laser, j'ai décidé de le faire à la Shaper avec sa fraise de 3mm en mode Gravure, soit avec une découpe d'une profondeur de 0,5mm. Mes premiers tests n'étaient pas concluants car la fraise de 3mm, la plus petite disponible, était toujours trop grande pour un dessin aussi petit et les différentes lignes se confondaient pour donner un dessin incompréhensible.

Je me suis donc rabattue sur l'idée de graver en pochant la silhouette globale du fourgon. Pour cela, j'ai utilisé la shaper avec une friase de 3 en mode Gravure à nouveau mais avec une profondeur de 1mm (car 0,5 ne se voyait pas assez, voir l'essai du haut de la photo 2 ci-dessous)à l'intérieur du trait puis en pochage. J'ai ensuite teinté la partie gravée avec une teinture bois couleur acajou pour respecter l'identité graphique.

![](./images/projet-final/test-final/logo-gravure.jpg)

<br>

## **Éléments emboîtables**

En diversifiant les techniques de fabrication de ces éléments, cela permet à tout le monde de pouvoir fabriquer celui qui lui convient avec ce à quoi il a accès !

Les exemples d'éléments déjà modélisés sont téléchargeables en suivant [ce lien](https://a360.co/3IG1KrN).

### Impressions 3D

Pour l'impression 3D, j'ai conçu 3 exemples : un porte-gobelet à double-étage, un porte-tasse et un bol.

#### Porte-gobelet

Le porte-gobelet dispose d'un premier emplacement de 70mm de hauteur afin de maintenir une gourde stable, et un second de 15mm qui sert également de fond de poche pour la monnaie ou pour une cannette plus étroite.

Pour le modéliser, j'ai créé une esquisse d'un carré de dimension du paramètre "côtéboîte" que j'ai extrudé de 5mm vers le haut en choisissant bien "new body". Cette première étape consiste l'étape de base pour toute modélisation d'éléments emboîtables car elle constitue le socle qui maintient l'élément sur le module.
Sur cet objet, j'ai créé une nouvelle esquisse d'un premier cercle de diamètre "côtéboite - 2 * epbois - 5mm" et un second de "côtéboite - 2 * ebois - 5 - 3mm" afin de créer une paroi de 3mm que j'extrude tout d'abord vers le bas en opération "cut" puis une seconde fois de -70mm en "join". Je fais une nouvelle esquisse sur la tranche inférieure de cette paroi circulaire et je trace un cercle suivant le tracé extérieur que j'extrude de 5mm. Sur la face inférieure de ce cercle extrudé je fais une nouvelle esquisse avec à nouveau deux cercles, un de diamètre "côtéboite - 2 * epbois - 5 - 15mm" afin de créer un débord de 15mm pour poser notre gourde, et un de diamètre "côtéboite - 2 * epbois - 5 - 15 - 3 mm" pour comme la fois précédente extrudée une paroi de 3mm tout d'abord en opération cut puis en opération join de 10mm. Je répète l'opération de l'esquisse d'un cerle sur le périmètre extérieur que j'extrude de 5mm vers le bas pour créer le fond.

![](./images/projet-final/test-final/porte-gobelet-modeli.jpg)

J'ai utilisé les mêmes paramètres d'impression présentés précédemment pour le dernier test de porte-gobelet réalisé et l'impression a duré 9h à vitesse de 100%.

![](./images/projet-final/test-final/porte-gobelet-imp.jpg)

#### Repose tasse

Le repose tasse a été conçu selon les mêmes étapes que le porte-gobelet, il est d'un diamètre de 85mm et d'une profondeur assez faible de 30mm afin de ne pas gêner la anse et qui permet donc de poser sa tasse ou son mug. J'ai conservé toujours les mêmes réglages d'impression et celle-ci a duré 4h50 et a parfaitement fonctionné.

![](./images/projet-final/test-final/repose-tasse.jpg)

#### Bol

Le bol permet d'y mettre des fonds de poche, des petits aliments à grignoter sur la route comme des noix ou des bonbons, des écouteurs etc. Il fait ici un diamètre de 88mm et une profondeur de 44.

Pour le réaliser, j'ai fait la première étape du socle de base sur lequel j'ai fait une esquisse d'un cercle de diamètre "côtéboite - 2 * epbois - 5mm" et une sphère de même diamètre que j'ai scindé en 2 avec pour plan de coupe la surface du haut du socle. J'ai créé une seconde sphère avec le même centre mais de diamètre "côtéboite - 2 * epbois - 5 - 3mm" en opération "Cut".

![](./images/projet-final/test-final/bol-modeli.jpg)

Cette impression a duré 5h30. Ma première impression a eu quelques soucis au niveau de la dernière couche qui a gondolé. J'ai effectué un deuxième essai qui lui a fonctionné.

![](./images/projet-final/test-final/bol-fini.jpg)


### Thermoformage

Le thermoformage permet de créer un élément parfaitement adapté à un objet en particulier que l'on possède en épousant sa forme pour en faire un moule très rapidement et facilement. Pour cet exemple, j'ai choisi de faire un porte-gourde adapté à la mienne. Ma gourde étant trop grande pour la thermoformeuse, j'ai cherché chez moi un objet qui correspond au diamètre de ma gourde : un petit pot en verre. J'ai également thermoformé d'autres objets comme un petit pot de fleur de 7cm de diamètre, un petit tupperware et un fond de bouteille en plastique.

Pour réaliser le thermoformage, il suffit de pincer une feuille de plastique conçue pour la thermoformeuse entre les deux plateaux, attendre qu'elle soit chaude, lorsqu'elle est prête elle se bombe vers le bas, on descend alors le plateau fermement (mais pas brusquement) sur notre objet et on allume l'aspirateur pour aspirer l'air et permettre au plastique d'épouser l'objet.
Pour mon premier test j'avais allumé l'aspirateur avant de descendre le plateau ce qui a fait que le moule n'était pas parfait. Le second a très bien fonctionné. Les autres thermoformages également sauf celui de la bouteille car le plastique de la bouteille a fondu sous la chaleur de la feuille de plastique et les deux ont donc fusionné.

![](./images/projet-final/test-final/thermoformage_1.jpg)

J'ai ensuite coupé la surface plate de la feuille des différents éléments thermoformés pour qu'ils correspondent au carré de mon module de 12cm de côté et je les ai bombé de la couleur de mon van pour coller à l'identité graphique de mon objet.

![](./images/projet-final/test-final/thermoformage_2.jpg)

### Découpe

À la découpe laser, shaper ou même avec tout type de scies, il est possible de réaliser différents éléments comme un double-fond pour cacher des objets ou des parois de séparations pour le module.

J'ai découpé dans la même planche en sapin que pour le module, des cales permettant de créer un double fond et le fond dans une plaque d'isorel pour ne pas perdre trop de volume.

La largeur des cales correspond à la largeur du module à laquelle on soustrait deux fois l'épaisseur du bois. Ici, 120 - 24mm soit 96mm. La hauteur dépend de celle désirée pour le double fond. Ici, 65mm pour le plus grand côté. Le fond est un carré de même largeur que les cales.

Afin de maintenir les deux cales à la verticale, j'ai essayé deux façons différentes. La première consiste à fixer le double fond à chaque cale à l'aide de charnières très fines, mais l'isorel étant très fin la réalisation est assez compliquée et on ne peut pas ouvrir le compartiment entièrement.

![](./images/projet-final/test-final/double-fond-module.jpg)

La seconde consiste elle à venir fixer les cales sur un second fond à la colle à bois. Cette façon permet de facilement retirer le compartiment entier du module.

![](./images/projet-final/test-final/double-fond-2.jpg)

<br>

### **Résultat final**

![](./images/projet-final/test-final/objet-final.jpg)

# **ÉTAPE 6 :** Un nom, un pictogramme

Je vais terminer cette documentation par l'étape finale : celle de l'identité visuelle de la Pop'o box.

Après avoir brainstormer différentes façons de créer une identité graphique pour mon objet, j'ai décidé de réaliser les éléments emboîtables dans la (future) couleur de mon van : *Assuan Brown*, une couleur d'origine du T3 proche de la teinte terracotta de code #824330. À ça s'ajoute le logo ancré dans le module en bois : la silhouette mythique du VW T3, rappelant que ce module est conçu et adapté pour les tableaux de bord des T3, un des vans favoris des vanlifers depuis maintenant 40 ans.

Pourquoi **Pop'o box** ? Le principe de ce module est d'avoir des petites boîtes de rangement qui s'accrochent partout et qui peuvent se 'pop !' ensemble, le surnom officiel du modèle T3 étant *Popo*... voilà la Pop'o box. Concept que l'on retrouve également dans son pictogramme, des cubes qui s'emboîtent pour faciliter le quotidien dans un van sur la route à suivre le soleil ! :)

![](./images/projet-final/test-final/logo_popobox.png)

<br>

# Liens utiles

- [Fichier .stl du module et ses compartiments](https://a360.co/3IG1KrN)
- [Fichier .svg du logo VW T3](./images/projet-final/test-final/silhouette_VWT3.svg)
- [Fichier .stl de la console](https://a360.co/3rOwJLx)
