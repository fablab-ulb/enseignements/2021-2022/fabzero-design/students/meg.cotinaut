# COURS DESIGN

Sur cette page se trouvent des résumés des cours d'introduction au Design et ce que j'en ai tiré. 

<br>

## 1. *Design or not Design*

Pour cette première semaine d'avancée sur notre projet, notre enseignant Victor Lévy nous a présenté une introduction au Design dont je vais parler en quelques mots pour introduire le processus de création de nos objets.

Comme on peut le voir notamment dans le travail de Venturi *L'enseignement de Las Vegas*, en architecture on peut trouver d'un côté un bâtiment sous la forme d'une simple 'boîte décorée' qui abrite une fonction particulière et de l'autre un bâtiment qui est un signe en lui même et qui agit alors comme un symbole. Cette notion se retrouve dans les objets et leur design : nous avons d'une part l'*outil* qui répond à un besoin ou une fonction, par exemple un marteau, et de l'autre le *signe*, qui est plus de l'ordre du décoratif, comme un nain de jardin. Pour notre travail de semestre, l'idée est de travailler plutôt sur l'aspect d'outil, car en travaillant sur un objet '*signe*' on risque d'oublier les réflexions sur la fonction et mettre de côté la réponse à un problème et des contraintes.

<br>

Pour Victor, il est important de concevoir notre objet en suivant un processus de 6 étapes, dont l'ordre importe beaucoup car il faut réfléchir à l'objet avant de le vendre plutôt que, comme à notre habitude en tant qu'étudiant en architecture, de le concevoir en étant déjà guidé par notre argumentation devant le futur jury. Ces étapes sont donc :

1. Déconstruction de la fonction
2. Résoudre un vrai problème
3. Comprendre les dimensions spécifiques
4. Esquisse, essai-erreurs, améliorations
5. Adaptation, diffusion, édition
6. Un nom, un pictogramme

La première étape consiste à déconstruire la **fonction de l'objet**, et non l'objet en lui même. Nous devons alors nous placer en tant que simple observateur qui regarde l'objet d'un regard extérieur, et non en tant que concepteur, par conséquent il s'agit de jouer le rôle de beaucoup d'acteurs différents afin d'**analyser et de comprendre l'utilité et les problèmes**. Ainsi, on apprend à avoir un regard différent sur notre objet, celui d'un psychologue ou d'un ergonome par exemple, afin de rentrer dans cette fonction première. Cette étape

La deuxième étape nous permet de mettre en place **les contraintes et les limites** qui vont faire l'objet et **structurer notre processus de création**. Pour cela, il est important de réellement se détacher de notre objet en prenant cette fois la place d'un juge impitoyable. C'est ce que Peter Opsvik explique lorsqu'il dit que la clé pour réaliser des bons objets est de trouver les faiblesses dans des solutions déjà existantes pour un problème : on trouve et analyse des contraintes et des limites qui vont nous permettre d'apporter quelque chose de nouveau et d'utile.

La troisième, nous devenons ergonome. En effet, il s'agit d'aborder **les dimensions relatives à l'objet**, comme celles du corps humain, des différents gestes, objets qui sont en lien avec ce dernier. Celles-ci sont importantes car **elles vont avoir un impact sur le confort d'utilisation**, tout comme en architecture. Chaque objet a des règles de dimension qui lui sont propres et dont il faut prendre connaissance pour créer un objet adapté, c'est ce qu'on retrouve dans le travail d'Opsvik à nouveau lorsqu'il teste toutes les positions possibles et imaginables sur une chaise.

La quatrième nous permet de prendre la casquette d'un concepteur afin d'entrer dans **les premiers essais et erreurs** de notre projet. Avec les techniques et machines disponibles au FabLab, nous devons faire et refaire, tester et retester, demander des avis extérieurs, tout cela afin que **chaque essai mène à des améliorations pour le suivant**. On trouve trois types de prototypes, ceux que l'on peut appeler 'sales' (des petits tests de fonctionnement qui peuvent être réalisés à la main), les 'propres' (maquettes) et les fonctionnels (qui sont donc des versions de notre objet qui fonctionne et qui ne sont pas juste un modèle de représentation).

La cinquième étape est celle qui nous rappelle que notre objet doit **être unique, diffusable et reproduisible**, notamment grâce à la documentation que nous effectuons sur le GitLab ainsi qu'un **mode d'emploi**. Il faut réduire notre objet à sa plus simple expression en utilisant un minimum de matière. C'est également lors de cette étape que peut se poser la question des séries ou collections.

Finalement, la dernière étape est celle du **nom et logo**. Ils doivent être lisible rapidement et facilement, rendre compréhensible et apporter des informations sur l'objet en offrant un contexte en le liant à notre univers. Ils sont **l'identité de notre objet**.

<br>

Nous avons vu beaucoup d'exemples intéressants, du bac à litière aux vélos électriques, mais ceux qui m'ont le plus marqué sont tout d'abord le [*Kitchen Stories*](https://fr.wikipedia.org/wiki/Kitchen_Stories) qui m'a appris que pour concevoir les premières cuisines IKEA, les concepteurs passaient des heures et jours à observer des suédois dans leur cuisine pour connaître leurs habitudes et besoins, mais aussi l'imperméable [*Freecut* de Muji](https://www.muji.eu/pages/online.asp?Sec=5&Sub=130&PID=11865&qclr=4550002978631&qsiz=967&qsel=) qui est donné en trois tailles S,M,L qui dictent uniquement la largeur car la longueur du vêtement et des manches est laissée au plus long avec des tracés permettant de le couper aux longueurs adaptées à nous et rendant l'objet beaucoup plus inclusif et avec des contraintes qui viennent réellement s'inscrire dans l'objet en lui-même.

<br>

## 2. *Design : le rapport à la personne*

Pour ce deuxième cours de Victor sur le design, il nous a parlé de plusieurs **exemples de chemins pour faire un objet et du rapport à la personne qu'entretient chaque design d'objet*.

Nous avons tout d'abord vu l'exemple de son objet qui pour moi était le plus parlant, dans la même idée que les objets que nous concevons lors de ce quadrimestre. Grand amateur de cuisine asiatique, il voulait pouvoir concevoir un bol qui répondait parfaitement à ses besoins et envies pour manger ses plats. En partant de bols qu'il possède déjà, il a pu en tirer les bons points et ceux qui posaient problèmes pour proposer différents prototypes. Le bol de départ (son préféré) est un bol japonais en porcelaine, de là, il l'a reproduit à l'impression 3D en retirant le pied, en faisant ça il s'est rendu compte que le bol tenait alors dans la main et que ce côté était avantageux car c'est une pratique courante en Asie, mais trop petit pour manger la quantité souhaitée. Le prototype suivant, en impression 3D également, était donc le même mais de plus grande taille. Le suivant de même taille mais avec deux surfaces planes, une en dessous pour le poser, et une décalée permettant de le poser de façon inclinée pour pouvoir apercevoir facilement ce qu'on a dans les bols. Il est important de préciser que ces derniers ne sont pas des prototypes fonctionnels car ils ne sont pas en plastique *food safe* et donc inutilisables. Pour effectuer des prototypes fonctionnels se pose alors la question du matériau utilisé : en céramique ? avec une imprimante résine ?

Ce que je tire de cet exemple, c'est le procédé de *try and error* que Victor a suivi. **De chaque essai il tire des nouvelles contraintes et des choses à améliorer pour arriver petit à petit à l'objet le plus adapté et répondant le mieux à son besoin de départ.** Sans effectuer ces tests et prototypes, il n'aurait pas pu se rendre compte que le bol était trop petit ou qu'il pouvait le poser de façon inclinée pour présenter ses plats sur sa table.  

Nous avons vu d'autres exemples de procédés de conception de certains objets comme le porte-GSM universel, de la protection contre la pluie à vélo pour les jambes ou encore le sifflet cubin minuscule qui prend en compte le corps dans la conception même de l'objet qui ne fonctionnerait pas sans les doigts de la personne de chaque côté.

<br>

Au cours de cet échange, j'ai également pu discuter et réfléchir sur les premières étapes de mon objet : peut-être des éléments pluggables ? combiner les utilités en un seul compartiment (par exemple un porte gobelet avec un fond pour les pièces de monnaie également) ?

Cet échange m'a permis de réaliser **qu'il faut réellement revenir à la fonction première de l'objet** : tenir en place des objets. Il faut **se détacher de la forme** pour pouvoir imaginer des possibilités répondant à cette fonction.

<br>

## 3. *Point méthodologie*

Cette semaine, nous avons fait un point rapide sur la méthodologie pour notre projet par rapport au processus de 6 étapes expliqué dans la première partie de cette page. 

Arrivé à l'étape 4 d'esquisse et d'essais, il faut toujours refaire une vérification de la définition, qui nous permet de nous rappeler si oui ou non nos idées et prototypes correspondent toujours à cette définition, permettant de rajouter une ou plusieurs contraintes à celle-ci grâce à nos essais et erreurs, et de regarder ensuite dans l'existant, dans d'autres technologies, réfléchir à la matière adaptée. 
Une autre 'sous-étape' de cette étape 4 est, si besoin, de se tourner vers un expert, pour comprendre le fonctionnement de l'objet ou résoudre un problème que l'on rencontre, comme par exemple avec Joana qui s'est tournée vers un kiné pour réellement comprendre quelle position du poignet est adaptée ou mauvaise à l'utilisation d'un ordi.

Il s'agit bien d'une vérification et pas d'un 'jury' ou un d'un 'juge' tout simplement parce que en tant ue vérificateur on peut évaluer comment l'améliorer et comprendre la fonctionnalité de chaque test même raté comme une avancée dans le projet. Cela permet également de penser à son adaptabilité et sa possible diffusion. 
