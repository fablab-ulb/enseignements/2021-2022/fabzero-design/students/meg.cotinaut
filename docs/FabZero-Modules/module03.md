# 3. Impression 3D

Cette semaine, notre troisième module s'articulait autour de l'impression 3D. Au FabLab, on trouve plusieurs imprimantes 3D Prusa : des modèles i3 MK3 et i3 MK3S/MK3S+. Afin de prendre en main leur utilisation et la préparation sur le logiciel au préalable, nous avions pour exercice de réaliser notre objet ou une partie de l'objet réalisé au module 2 sur Fusion 360 -en taille réduite si nécessaire- de façon à ce que l'impression dure moins de 2 heures.

<br>

## **L'imprimante Prusa**

L'entreprise Prusa Research, à l'origine des imprimantes Prusa, a été fondée en 2012 comme startup par Josef Prusa, un hobbyiste, maker et inventeur tchèque. Leur idéologie était de créer des machines open-source, ainsi bon nombre de leurs composants sont imprimables par d'autres imprimantes 3D : tous les codes sources, pièces imprimées, plans, conceptions de cartes de circuits imprimés etc sont disponibles gratuitement grâce au GitHub, et la communauté partage son savoir sur [PrusaPrinters.org](https://www.prusaprinters.org/#_ga=2.235507273.994300260.1634330638-22385276.1634330638).

![Prusa i3 MK3](../images/module-3/11_3dprinter.jpg)

*Photo issue du site [Prusa 3D](https://www.prusa3d.com/page/prusaslicer_424/)*

Le filament est chauffé et fondu à la sortie de l'extrudeur afin d'être déposé sur le lit, les deux se déplaçant selon les axes x,y,z afin de former la pièce.

- N°1 : les bobines de filaments. Plusieurs types et couleurs de filaments et de plastiques peuvent être utilisés pour l'impression, le principal étant du PLA. Il passe alors dans l'extrudeur et l'élément chauffant qui le fait fondre.
- N°2 : le plateau chauffant, qu'il faut nettoyer à l'acétone avant chaque utilisation afin de permettre à l'objet imprimé d'avoir une meilleure adhérence au plateau, et donc éviter qu'il se renverse ou bouge lors de l'impression. Son aspect chauffant permet également d'augmenter l'adhésion de la pièce imprimé.
- N°3 : le panneau de contrôle, permettant de régler différents paramètres d'impression, changer et charger le filament, voir le temps restant etc

<br>

## **Le logiciel PrusaSlicer**

**Afin de préparer le fichier d'impression à utiliser dans l'imprimante, nous travaillons sur le logiciel PrusaSlicer pour pouvoir paramétrer les épaisseurs, remplissages, supports et autres réglages. Pour cela, il suffit de convertir le fichier Fusion360 de votre objet en fichier *.STL* et de l'ouvrir sur PrusaSlicer.**

![Espace de travail PrusaSlicer](../images/module-3/00_prusaslicer.jpg)

Le plan de travail (1) représente le plateau chauffant de l'imprimante sur lequel on peut visionnner notre objet et avoir un aperçu de l'impression. Il existe le mode éditeur et le mode prévisualisation (2), ce dernier permettant d'avoir un aperçu de l'impression en détail.
En mode éditeur, plusieurs outils (3) permettent de déplacer l'objet ou de le faire pivoter, changer son échelle, le placer à plat sur une face etc.
Pour ce qui est des réglages, la plupart se font par défaut en choisissant l'imprimante utilisée (4). Il faut également choisir le matériau utilisé. On peut y modifier l'échelle de l'objet et voir le temps estimé d'impression. Il est possible de faire des réglages plus en détails de l'impression, du filament et de l'imprimante (5) comme créer un support, une jupe, modifier le motif et pourcentage de remplissage, le nombre de couches etc.

<br>

## **Impression de mon objet**

### 1. Paramétrage sur le logiciel PrusaSlicer

1/. J'ai donc tout d'abord décidé d'imprimer uniquement un porte-gobelet de la tablette, que j'ai isolé sur Fusion360 en réalisant un nouveau sketch sur la face du haut avec 3 rectangles comme sur l'image ci-dessous que j'ai ensuite extrudé pour faire disparaître tout l'objet sauf le porte-gobelet. Je l'ai ensuite exporté en stl.

Le fichier Fusion 360 du porte-gobelet est disponible sur *[ce lien](https://a360.co/3bcYcPj)* et vous pouvez directement télécharger le fichier .stl en *[cliquant ici](../images/module-3/porte-gobelet-test.stl)*.

![Sketch cup holder](../images/module-3/00_sketchcup.jpg)
![Extrude](../images/module-3/00_extrudecup.jpg)

![Objet sur Fusion360](../images/module-3/01_cupfusion.jpg)
![Objet sur PrusaSlicer](../images/module-3/01_cupslicer.jpg)

2/. Une fois sur le logiciel, il faut mettre l'objet à plat sur le plateau. La commande Place on Face (F) permet ça en un clic : il suffit de cliquer sur la face que l'on veut mettre à plat, pour mon objet il s'agit de la face du dessous.

![Objet face down](../images/module-3/02_facedown.jpg)

3/. Pour réduire le temps d'impression de ce premier prototype de test, j'ai réduit l'échelle de mon objet à 40% dans le menu latéral. En réduisant l'échelle directement sur PrusaSlicer, les proportions sont conservées.

![Scale](../images/module-3/03_scale.jpg)

4/. Dans les réglages généraux de ce même menu, je sélectionne le paramètre d'impression correspondant à mon imprimante, le filament choisi (ici, Prusament PLA) et le type d'imprimante comme ci-dessous :

![General settings](../images/module-3/04_generalsettings.jpg)

5/. Dans l'onglet *Print Settings*, je règle tout d'abord les paramètres de couches *Layers and perimeters*. Sachant que mon objet est un test et qu'il est de très petite taille, je règle le périmètre des couches verticales et horizontales sur 2, par défaut le réglage est souvent de 3, si l'on veut le rendre plus solide ou le rendre étanche il faut augmenter ces couches. En choisissant les mêmes paramètres pour les deux, l'objet sera d'épaisseurs homogènes.

***Conseil :** au début je ne trouvais pas l'accès à tous les réglages nécessaires, pour cela il faut passer en mode "Expert" !*

6/. Je crée également un *Raft* de 3 couches : cela vient créer un support plus large à la base de mon objet qui sera détachable mais qui permet d'augmenter sa surface d'adhérence au plateau chauffant et éviter qu'il bouge ou se renverse. *Vous pouvez le voir en vert sur le rendu avant impression à l'étape 8.* Je n'ai pas besoin d'autres supports vu la simplicité de la forme de mon objet, sa taille, sa surface au sol et l'absence de ponts.

7/. Je règle le remplissage en motif de grilles de 5% car une fois de plus, l'objet est de petite taille et ne nécessite pas une grande solidité. Pour le rendre plus solide, on peut augmenter son pourcentage de remplissage et opter pour un motif plus compact ou résistant.

![Print Settings](../images/module-3/05_settings.jpg)

***Conseil :** pour visualiser le rendu des paramètres de remplissage choisis ou comprendre l'impression, il est possible de se déplacer sur la TimeLine d'impression à droite de notre objet dans l'onglet de visualiation *Plater*. Ici, vous pouvez voir à quoi ressemblera le le remplissage en grilles de 5% que j'ai choisi.*

![Prévisualisation remplissage](../images/module-3/06_grid.jpg)

8/. Une fois tous les réglages effectués, je clique sur *Slice now* et je peux voir le temps d'impression estimé. J'exporte alors le fichier en G-code que je glisse sur la carte SD de l'imprimante.

![Prévisualisation impression](../images/module-3/07_finalslicer.jpg)

<br>

### 2. Préparation de l'impression

**Une fois votre fichier paramétré et converti en G-code, il est temps de passer à l'impression avec quelques vérifications au préalable.**

1/. Je vérifie qu'il y ait suffisamment de filaments restants sur la bobine (et je le change si ce n'est pas le cas ou si je veux différents matériaux ou couleurs). J'ai choisi d'utiliser du PLA de couleur noire afin de donner le même rendu que l'objet original.

2/. Je nettoie le plateau aimanté comme expliqué dans la présentation de l'imprimante.

3/. J'ouvre mon fichier que j'ai placé sur la carte SD depuis le panneau de contrôle.

![Panneau de contrôle](../images/module-3/08_printersettings.jpg)

4/. Sur ce panneau, je mets la vitesse à 100% pour débuter, il est possible, par la suite et durant l'impression, de l'augmenter par palier de 50% (et à maximum 300%). Je vérifie également que l'impression débute bien à une hauteur Z de 0,2. Elle débutera une fois que la température indiquée est atteinte !

***Conseil :** il est toujours préférable de rester surveiller l'impression pour les 3 premières couches afin de vérifier que tout se passe bien et que nos paramètres sont bons.*

<br>

### 3. L'impression

L'impression de mon objet s'est très bien passé du premier coup et elle a duré 1h16, sachant que je l'ai laissé à la vitesse de 100% tout le long.

L'impression du support en un premier temps, puis de l'objet avec son remplissage en grille :

![Impression support](../images/module-3/09_supportprinting.jpg)
![Impression objet](../images/module-3/10_objectprinting.jpg)

Et voilà, votre porte-gobelet de tableau de bord version miniature est terminé !

![Objet imprimé face](../images/module-3/photo_01.jpg)
![Objet imprimé dos](../images/module-3/photo_02.jpg)
![Objet imprimé dessous](../images/module-3/photo_03.jpg)

<br>

## **Liens utiles**

- [Site des imprimantes Prusa](https://www.prusa3d.com)
- [Communauté PrusaPrinters](https://blog.prusaprinters.org/#_ga=2.42844909.994300260.1634330638-22385276.1634330638)
- [Télécharger PrusaSlicer](https://www.prusa3d.com/prusaslicer/)
- [Fichier fusion 360 du porte-gobelet](https://a360.co/3bcYcPj)
- [Télécharger le fichier .stl](../images/module-3/porte-gobelet-test.stl)
