# 5. Usinage assisté par ordinateur

Pour cette dernière formation, nous avons été introduit à un outil d'usinage assisté par ordinateur qui est le Shaper Origin.

<br>

## **Shaper Origin : la machine**

### La Shaper Origin

![ShaperOrigin](../images/module-5/shaperorigin.jpg)

*Photo issue du site [Shaper Origin](https://www.shapertools.com/fr-fr/origin/spec).*

La Shaper Origin est une **fraiseuse CNC portative de précision**, guidée à la main.

**Informations utiles :**

- Profondeur de découpe max. : 43 mm
- Diamètre du collet : 8 mm ou 1/8"
- Format de fichier supporté : SVG

Pour une vue d'ensemble de la machine, *[cliquez ici](https://www.shapertools.com/fr-fr/origin/spec)*, pour le manuel d'utilisation, c'est *[ici](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf)*.

Cette fraiseuse permet de découper des formes de manière très précise dans du bois grâce à une fraise dont la machine corrige la trajectoire lorsque l'on guide l'outil sur le matériau en suivant le tracé affiché sur l'écran.

**Quelques précautions d'usage :**

- Utiliser la machine sur un plan de travail stable
- Toujours fixer le matériau sur le plan de travail à l'aide de double face, serre-joint ou vis
- Ne pas oublier d'allumer l'aspirateur à poussières
- Être dans une position stable, équilibrée et confortable pour l'utiliser, et veiller à ce que l'ensemble de la découpe soit à notre portée
- Éviter les vêtements larges, bijoux, ou cheveux longs lâchés

![Composition ShaperOrigin](../images/module-5/schema_shaperorigin.jpg)

*Photo issue du site [Shaper Origin](https://www.shapertools.com/fr-fr/origin/spec).*

La machine se compose d'un corps principal avec une poignée pour la déplacer facilement, un **port usb** pour importer notre fichier, un **mandrin** et la fraise, un **écran tactile** qui permet de paramétrer et visualier la découpe, deux **poignées latérales** permettant de démarrer/arrêter et guider la découpe, ainsi qu'une **caméra** avec lumières LED.

En effet, pour réaliser une découpe, la machine utilise des repères par sa caméra : le **Shaper tape**.

![Shaper Tape](../images/module-5/shapertape2.jpg)

*Photo issue du site [Shaper Origin](https://www.shapertools.com/fr-fr/origin/spec).*

Il faut placer des bandes parallèles de ce dernier tous les 8cm devant la machine qui utilise la caméra pour les scanner et pour se définir l'espace de travail. Elle montre également en temps réel le placement de la découpe puis le tracé à suivre tout au long de cette dernière.

**Attention !** Il faut veiller à ce que le ShaperTape couvre toute la zone que l'on prévoit de découper et devant cette zone afin que la Shaper Origin capte toujours ces marqueurs.

<br>

### Réglages

La machine propose différentes vitesses allant de 1 à 6, qui correspondent à une vitesse de 10 000 à 26 000 tours/minute. Cette vitesse de fraisage dépend du matériau et de son épaisseur : une découpe trop lente ne sera pas lisse et laissera apparaître des à-coups tandis qu'une vitesse trop rapide brûlera ou écorchera le bois. Un indicateur de vitesse est la taille des copeaux lors de la découpe, la taille normale des copeaux est d'envrion 1mm, s'ils sont plus gros c'est que la vitesse est trop lente, plus petits alors trop rapide. Elle se règle sur **le cadran de contrôle de vitesse**.

L'option de **vitesse automatique** doit également être déterminée et permet à la machine de guider elle-même la découpe, auquel cas il faut pouvoir la suivre malgré tout, on évitera donc des vitesses trop rapides.

Il existe plusieurs fraises de différentes tailles pour différents matériaux et épaisseurs.

Sur *[ce lien](https://support.shapertools.com/hc/fr-fr/articles/360016398434-Recommandations-de-paramètres-par-matériau)* vous pouvez trouver **un tableau regroupant selon différents matériaux, la fraise, vitesse de la broche, vitesse auto et profondeur maximale adaptée**.

Il est important de prendre en compte **l'épaisseur de la fraise** dans la découpe et le tracé. 

Il existe différents types de découpes :

- *inside* : découpe à l'intérieur du tracé,
- *outside* : découpe à l'extérieur du tracé,
- *on line* : découpe sur le tracé
- *pocket* : coupe par évidage

![Types de coupes](../images/module-5/typedecoupe.jpg)
![Code types](../images/module-5/code.png)

*Photo issue du [guide du FabLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/Shaper.md).*

Il est possible de réaliser 2 à 3 passes afin d'obtenir une découpe propre et homogène. Il est notamment possible de régler un décalage de la découpe par rapport au tracé, *offset* qui permet par exemple d'évider les coins.

<br>

## **Utilisation**

Pour un guide pas à pas d'utilisation je vous recommande le *[guide du FabLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/Shaper.md)* ou *[les tutoriels vidéos](https://www.shapertools.com/fr-fr/tutorials)* de ShaperTools.

Pour nous permettre de comprendre la machine, nous avons réalisé la découpe d'une planète dans une planche de bois.

<br>

### Le dessin

La machine, tout comme nous l'avons vu la semaine précédente pour la découpe laser, nécessite une image vectorielle, soit des **fichiers svg**.

Ainsi, il faut réaliser notre dessin sur un logiciel de tracés vectoriels comme Illustrator ou Inkscape.

**Attention !** Pour une découpe, assurez-vous que votre tracé soit bien fermé et non-ouvert, auquel cas la machine ne réalisera pas une découpe complète de l'objet.

<br>

### La découpe

*Afin de préparer la découpe, il faut comme expliqué dans la partie précédente, veiller à bien fixer le matériau au plan de travail.*

1/. On place le *ShaperTape* comme expliqué précédemment, soit en bandes parallèles séparées de 8cm maximum et de minimum 4 rectangles et on met la fraise adaptée dans la machine.

**Conseil :** pour changer la fraise, il faut tout d'abord appuyer le bouton de verrouillage de la broche, puis dévisser la vis à l'aide de la clé T-Hexagonale afi nde retirer la broche.

![Placer Tape](../images/module-5/shapertape.jpg)  

2/. Dans le menu **Scan** à droite de l'écran, on clique sur *Nouveau scan*. Il faut ensuite bouger la Shaper Origin de façon à scanner tout le ShaperTape qui devient bleu lorsque ce dernier est enregistré par la machine.

![Scan](../images/module-5/00_scan.jpg)

3/. Il est possible de dessiner des formes simples, notamment pour effectuer des tests de calibrage de vitesse par exemple, directement depuis la machine dans le menu **Dessiner**.

![Dessin](../images/module-5/01_dessin.jpg)

4/. Dans ce même menu, en cliquant sur *Importer* et grâce au port USB, on importe notre fichier svg réalisé auparavant.

![Import](../images/module-5/02_importer.jpg)

5/. Une fois notre fichier sélectionné, on peut visualiser son emplacement et la place qu'il prendra sur l'espace de travail, changer son échelle, le tourner etc. Le curseur à droite permet d'agrandir ou rétrécir le point de vue sur l'espace de travail. Une fois satisfait de sa taille et emplacement, on clique sur *Placer*.

![Placement](../images/module-5/03_placer.jpg)

***Conseil** : en haut à droite de l'écran se situe un indicateur permettant de savoir si l'on se trouve dans l'espace possible de découpe déterminé par l'emplacement du ShaperTape et le scan effectué. Lorsque ce n'est pas le cas, le voyant apparaît en rouge, lorsque c'est partiellement le cas, en gris et noir, et lorsque l'on est bien placé, en noir.*

6/. Dans le menu de droite, on passe maintenant à l'étape **Fraiser**. Sur la gauche, les différents réglages comme la profondeur de fraisage que l'on met ici sur 6mm, le type de découpe qui est ici sur *outside*, l'offset qui est ici à 0, la dimension de la fraise etc. On s'assure également que toute la découpe est à notre portée. Lorsque tous les paramètres sont enregistrés on clique sur *Fraiser*.

![Fraiser](../images/module-5/04_fraiser.jpg)

7/. Avant d'effectuer la découpe, il ne faut pas oublier **d'allumer l'aspirateur** à poussières et de le relier à la machine à l'aide du tuyau adaptateur, et **d'allumer également la broche** grâce au bouton on/off.

![Aspirateur](../images/module-5/aspirateur.jpg)
![On off](../images/module-5/07_onoff.jpg)

8/. Pour lancer la découpe, on appuie une fois sur **le bouton vert** sur la poignée de droite. En restant appuyé, la machine passe en vitesse automatique.

![Bouton vert](../images/module-5/05_boutonvert.jpg)

9/. On suit la trajectoire affichée sur l'écran en suivant le sens de la flèche. La partie du tracé déjà découpée apparaît alors en bleu. Si l'on sort du cercle blanc, la découpe s'arrête automatiquement et le mandrin se relève. Pour arrêter la découpe soi-même, on appuie sur le bouton rouge sur la poignée de gauche.

![Découpe](../images/module-5/decoupe.jpg)

**Attention!** Entre chaque forme ou tracé différent, il faut arrêter le fraisage et relever le mandrin (bouton rouge).

![Bouton rouge](../images/module-5/06_boutonarret.jpg)

Une fois la découpe terminé, on pense bien à éteindre la broche en basculant le bouton sur off.

On peut voir sur notre test que pour la première découpe d'un des continents, nous n'avions pas paramétrer la découpe assez profonde par rapport à l'épaisseur du matériau, ce qui n'a pas permis à la fraise de transpercer la planche et donc d'effectuer une découpe complète en une seule passe. Nous avons ensuite régler le fraisage sur une plus grande profondeur.

![Objet terminé](../images/module-5/objetfinal.jpg)

<br>

## **Liens utiles**

- [Shaper Origin](https://www.shapertools.com/fr-fr/origin/spec)
- [Manuel d'utilisation](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf)
- [Tutoriels vidéos](https://www.shapertools.com/fr-fr/tutorials)
- [Guide d'utilisation du FabLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/Shaper.md)

