# 4. Découpe assistée par ordinateur

Pour ce quatrième module, nous avons été formé à la découpe laser qui permet de réaliser des découpes précises dans différents matériaux donnés à partir de dessins vectoriels. Pour nous exercer nous avions pour mission de penser et réaliser des lampes à l'aide uniquement des LaserCut du FabLab, après avoir réalisé des grilles de calibrages.

<br>

## **Introduction à la découpe assistée par ordinateur**

### Logiciels

La découpe assistée par ordinateur, lasercut, consiste à pouvoir découper mais aussi graver dans différents matériaux en se basant sur une image **vectorielle** et non matricielle afin que la laser procède vecteur après vecteur pour le tracé. En effet, le vectoriel est un mode de codage d'images sous forme de descriptions géométriques, des vecteurs, créant une ligne ou courbe allant d'un point à un autre, et non point par point comme le matriciel.

Ainsi le **format commun demandé sur les machines est svg**, pour cela vous pouvez réaliser vos dessins depuis tout logiciel de dessin vectoriel comme Illustrator ou Inkscape.

Pour s'assurer d'une découpe fine, il faut penser à régler l'épaisseur des traits de votre dessin sur 0,01 mm (depuis Inkscape).

![Trait](../images/module-4/02_eptrait.png)

Pour les deux machines, il faut associer une couleur différente à chaque élément nécessitant une action différente, c'est-à-dire des réglages de vitesse et puissance différents. Par exemple une couleur pour la découpe, une pour la gravure, si vous avez plusieurs gravures à différents réglages pour des rendus différents, il faut également différencier les éléments grâce à des couleurs. Ces couleurs permettront également de régler l'ordre des découpes et gravures. Il est conseillé de **commencer par les gravures ou découpes intérieures** avant de découper le pourtour de l'objet pour éviter qu'il bouge et que les gravures/découpes intérieures ne soient pas au bon endroit.

<br>

### Matériaux et précautions

Il faut, avant toute découpe, impérativement fermer le couvercle, activer l’air comprimé et allumer l’extracteur de fumée. Pendant la découpe, il faut rester à proximité de la machine jusqu'à la fin de la découpe afin de pouvoir arrêter la machine à l'aide du bouton d'arrêt d'urgence en cas d'imprévu ou dysfonctionnement.

| Matériaux recommandés | Matériaux déconseillées| Matériaux interdits |
| ------ | ------ | ------ |
| Bois contreplaqué (plywood / multiplex), Acrylique (PMMA / Plexiglass), Papier, carton, Textiles | MDF, ABS, PS, PE, PET, PP, Composites à base de fibres, Métaux | PVC, Cuivre, Téflon (PTFE), Résine phénolique, époxy, Vinyl, simili-cuir, Cuir animal |

<br>

## **Les machines**

### 1. Lasersaur

![Lasersaur](../images/module-4/lasersaur_machine.jpg)

*Photo issue du [Shamblog](https://theshamblog.com/a-tour-of-the-lasersaur-laser-cutter/).*

Guide d'utilisation de la Lasersaur disponible *[ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)*.

Cette machine a pour particularité d'être opensource.

**Informations utiles :**

- Logiciel : Drive BoardApp
- Surface de découpe : 1220x610mm
- Hauteur maximum : 120mm
- Puissance du laser : 100-120W
- Type de laser : Tube CO2 (infrarouge)

**Important** ! Avant de lancer toute découpe ou gravure il faut impérativement :

- fermer le couvercle de la machine,
- allumer le refroidisseur à eau,
- allumer l’extracteur de fumée
- ouvrir la vanne d’air comprimé

Vous pouvez trouver des exemples de réglages selon les matériaux et épaisseurs sur le *[wiki Lasersaur](https://github.com/nortd/lasersaur/wiki/materials)*.

Avant toute découpe, et après avoir placé le matériau dans la machine, il est impératif de déplacer la tête de découpe au bon endroit en relevant la lentille pour éviter tout risque de collision et en positionnant la tête au-dessus du matériau (bouton Move sur le logiciel de l'ordinateur) et d'ensuite régler la distance focale avec le support de 15 mm.
Ensuite, il faut réaliser les différents réglages selon les éléments et actions demandées, puis vérifier que la découpe ne dépasse pas le matériau en utilisant le bouton *flèches* à côté de Run.

Une fois le bouton Status en vert, vous pouvez lancer la découpe avec le bouton *run* !

<br>

### 2. Epilog Fusion Pro 32 + calibrages

![EpilogFusion](../images/module-4/00_epilog laser cut.jpg)

*Photo issue du site [Epilog Laser](https://www.epiloglaser.fr/machines-laser/série-de-lasers-fusion.htm).*

Guide d'utilisation de l'Epilog Fusion disponible *[ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/EpilogFusion.md)*.

**Informations utiles**

- Logiciel : Epilog Engraver
- Surface de découpe : 810x500mm
- Hauteur maximum : 310mm
- Puissance du laser : 60 W
- Type de laser : Tube CO2 (infrarouge)

**Important** ! Avant de lancer toute découpe ou gravure il faut impérativement :

- fermer le couvercle de la machine,
- activer l’air comprimé et l’extracteur de fumée (voir photo 1 ci-dessus)

Vous pouvez trouver des exemples de réglages selon les matériaux et épaisseurs sur le *[wiki EpilogFusion](https://github.com/nortd/lasersaur/wiki/materials)*.

Afin de prendre en main les machines et notre matériau qui est le polypropylène, nous avons tout d'abord réalisé deux tests de calibrage, un premier de découpes et gravures et un second de pliage, sur l'Epilog Fusion, je vais utiliser ces tests *pour vous expliquer le fonctionnement de la machine*.
Ces calibrages nous permettent d'apprendre les caractéristiques de notre matériau et de, par la suite, choisir les réglages adaptés à nos besoins.

![Calibrages](../images/module-4/03_pliage_decoupage2.jpg)

Pour un premier essai, nous avons récupéré la grille de calibrage que nous avons adapté aux réglages de la machine. Elle est disponible *[ici](../images/module-4/calibration_grid.svg).*

***Conseil :** pour télécharger un fichier svg, il faut faire un clic droit sur le lien et "Download linked file"*

Nous avons ensuite *Split by Color* afin de paramétrer, sur le logiciel Epilog Engraver, la puissance (allant de 4 à 100%) et vitesse (allant de 10 à 100%) correspondante pour chaque carré, qui a une couleur différente, en suivant la grille.

Pour le second, nous avons dessiné nous même une grille de calibrage afin de tester différents réglages de pliage. Elle est disponible *[ici](../images/module-4/calibration_grid_pliage.svg)*. Nous avons paramétrer pour chaque ligne de couleurs différentes, une puissance allant de 10 à 55% pour une vitesse commune de 75.

![Print](../images/module-4/04_print.png)

La première étape consiste à ouvrir les fichiers sur Inkscape depuis l'ordinateur connecté à la machine et de cliquer sur *Imprimer* vers *Epilog Engraver*.

![Epilog](../images/module-4/05_epilog.jpg)
![Placement](../images/module-4/06_placement.jpg)

(1) Dans le menu déroulant *Auto Focus*, il faut sélectionner *Plunger* pour les matériaux durs et assez rigides afin que le focus de la machine se règle lui-même sur la surface du matériau. Pour des matériaux fragiles ou très souples comme le textile, il faut alors sélectionner *Thickness*.

(2) Pour pouvoir effectuer les réglages de vitesse et puissance par couleur pour chaque élément, cliquez sur *Split by : Color*.

(3) Pour réaliser des découpes et gravures vectorielles, il faut sélectionner pour chaque élément *Vector* dans *Process Type*, et *Engrave* pour les gravures d'images matricielles. Il faut également choisir les différents paramètres de puissance et vitesse.

On place ensuite la découpe sur notre matériau en cliquant sur la caméra, *Preview*, *Edit*. Et on envoie le fichier vers la machine en cliquant sur *Print* (4). Pour lancer la découpe, il faut sélectionner le fichier dans la liste apparue sur la machine puis cliquer sur le bouton Play (photo 2 ci-dessous). Le temps d'impression restant apparaît alors sur l'écran.

![Découpe calibrages](../images/module-4/07_decoupe.jpg)

<br>

## **Conception d'une lampe en polypropylène**

### Premiers pas

L'exercice principal consistait donc à concevoir et fabriquer une lampe en polypropylène uniquement en rapport avec notre objet choisi et en partant d'une source lumineuse. Les contraintes étaient de respecter les caractéristiques du matériau : son aspect translucide et sa souplesse, ainsi que les dimensions d'une feuille de 500 par 700mm, et finalement, d'avoir le minimum de déchets/pertes dans cette feuille.

![Moodboard](../images/module-4/10_moodboard.jpg)

Par conséquent, j'ai choisi pour source lumineuse la lampe frontale qui m'accompagnait pendant mes voyages. Son inconvénient : elle est difficilement utilisable autrement que sur la tête de par sa forme et son faisceau lumineux étroit et rectiligne.

Mon but était donc de concevoir une lampe permettant de diffuser sa lumière en étant soit suspendue (dans une tente ou le van) ou posée. Mais, comme la vie en van ou en backpacking demande une économie de place énorme, elle doit également être très peu encombrante ou pliable.

<br>

### Premières idées

Mes premières idées et esquisses reposaient sur une lampe assez carrée et pliable. Le système de pliage supérieur permettait alors de passer de par et d'autre les lanières de la lampe frontale afin de la suspendre ou la poser.

Le problème principal était que par une forme avec de nombreux angles et arrêtes, ainsi que l'orientation du faisceau lumineux vers une surface plane carrée, la lumière était très peu diffusée et uniquement en une direction.

![Premières idées](../images/module-4/11_idees1.jpg)

![Premiers test](../images/module-4/11_test1.jpg)

<br>

### Idée finale

J'ai donc testé de nouvelles idées avec des surfaces courbes. En courbant un simple carré dans la matière, on peut voir que la lumière est bien mieux diffusée et dans plusieurs directions (4), mais la forme est contraignante car la lampe frontale possède 3 lanières et les deux trous sur les côtés ne diffusent pas la lumière.

C'est suite à ces questions que je me suis orientée vers une base triangulaire qui permet de créer 3 passages pour les lanières et de diffuser la lumière non seulement en face mais également dans 3 directions latérales (5). Mais avec un simple triangle (5.2), en courbant les trois côtés, la lampe était très plate et par conséquent la source lumineuse très proche de la base en polypropylène ce qui empêchait une bonne diffusion de la lumière. Afin de contrer ce problème, j'ai tout d'abord réalisé des tests de gravures (en me basant sur la grille de pliage réalisée) permettant de légèrement plier le matériau et donc une meilleure courbure à chaque angle de la base (voir photo 2 ci-dessous). Puis, j'ai modifié le patron en créant une base plane triangulaire à laquelle se rattache trois autres triangles plus étroits (5.3) afin d'allonger la lampe et donc éloigner la source de la surface plane qui lui fait face.

![Dernières idées](../images/module-4/12_idees2.jpg)

![Derniers tests](../images/module-4/12_test2.jpg)

J'en ai profité également pour tester des systèmes d'attaches permettant de monter et démonter la lampe rapidement mais sans en perdre la solidité. J'en suis venue à ce système où la troisième branche maintient les deux premières attachées ensemble.

![Attaches](../images/module-4/13_attaches.jpg)

À titre de comparaison et d'illustration de tous ces tests, voici l'éclairage obtenu respectivement par la lampe frontale uniquement, une surface plane non courbée (esquisses 2 et 3), un carré courbé (esquisse 4) et le dernier modèle (esquisse 5.3).

![Tests éclairage](../images/module-4/14_eclairagetest.jpg)

<br>

## **Réalisation de la lampe à la lasercut**

### Dessin de la lampe

J'ai fait le choix de dessiner ma lampe sur Autocad car je maîtrise mieux ce logiciel qu'Illustrator ou Inkscape.

![Calques](../images/module-4/15_calques.png)

Avant toute chose, je crée deux calques distincts et surtout de couleurs de tracés différentes : un pour les découpes et un pour les gravures.

![Dessin autocad 1](../images/module-4/15_dessin_autocad_1.jpg)

1/. Je trace un triangle équilatéral de 120mm de côtés et d'angles de 60° avec l'outil *ligne*.

2/. Au milieu de la base du triangle, je trace une ligne perpendiculaire de 140mm.

3/. Je dessine deux lignes partant des deux côtés de la base du triangle au sommet de la ligne créée à l'étape précédente que je supprime.

4/. Je sélectionne les deux lignes créées que je copie et colle aux deux autres coins du triangle de la base avec la commande *copier*.

5/. Pour chaque couple de lignes collé, j'utilise la commande *rotation* pour les tourner afin d'avoir 3 triangles égaux de chaque côté de la base.

6/. Je sélectionne la base (le triangle équilatéral) que je place dans le calque gravure et les trois autres triangles dans le calque découpe comme sur l'image ci-dessus. Voilà la forme générale de la lampe !

![Dessin autocad 2](../images/module-4/15_dessin_autocad_2.jpg)

7/. Sur la même base que les étapes précédentes, je dessine mes attaches avec l'outil *ligne*, d'une épaisseur de 1,2 mm pour les encoches et 2 pour celle qui vient les maintenir (car le matériau fait 1mm d'épaisseur).

8/. Je sélectionne la base du triangle central et avec la commande *copier*, j'en ajoute 3 en dessous à intervalle régulier de 1mm. Je sélectionne les lignes créées et comme pour la phase précédente, je les copie/colle aux deux mêmes coins de la base.

9/. Une fois de plus, comme à la phase précédente, j'utilise la commande *rotation* pour les orienter selon la ligne de base du triangle concerné.

10/. Avec l'outil *Ajuster*, je sélectionne toutes les lignes de mon dessin et en cliquant sur les parties dépassant de ma forme je supprime ces extrémités et j'ajuste mes lignes de gravure au contour général de la lampe.

Voici le dessin de la lampe terminée. Je l'exporte en format dxf, que j'importe ensuite sur Inkscape afin de créer un fichier svg pour la découpe. *[Ciquez ici](../images/module-4/final lamp meg.svg)* pour télécharger le fichier svg du modèle de la lampe.

![Dxf input](../images/module-4/16_dxf_input.jpg)

<br>

### Découpe de la lampe

Je règle la taille de ma feuille en fonction de celle du matériau, soit 500x700mm.

![Taille feuille](../images/module-4/01_taillefeuille.jpg)
![Inkscape final](../images/module-4/17_inkscape.jpg)

Comme expliqué dans la partie précédente consacrée à l'Epilog Fusion, j'envoie mon fichier vers le logiciel Epilog Engraver de l'ordinateur de la machine et je suis les étapes indiquées. Je divise par couleur et paramètre alors pour chaque couleur, en me basant sur les calibrages réalisés précédemment :
- pour la découpe en rouge, je choisis une puissance de 20% pour une vitesse de 10%
- pour la gravure en vert, une puissance de 35% pour une vitesse de 75%

![Découpe lampe](../images/module-4/18_decoupe_lampe.jpg)

Et voilà la lampe finalisée !

![Lampe finale](../images/module-4/19_lampe_finale.jpg)

Un problème que je rencontre avec ce modèle final et que je conseille de modifier si vous venez à le réaliser, c'est la profondeur des deux encoches au niveau de l'attache.

En effet, je les ai dessiné sur plus de la moitié de la largeur de la bande et comme le matériau est assez fin et donc fragile, l'attache n'est pas très solide et risquerait, avec le temps et les usages, de finir par se déchirer. Pour l'instant, la troisième encoche qui les maintient ensemble évite cette déchirure mais il serait plus sûr de dessiner des encoches plus courtes, maximum de la moitié de la largeur voire moins, pour consolider l'attache.

Un autre petit problème survenu est que la lampe frontale que j'ai utilisé pour concevoir ma lampe et la tester n'était pas très puissante et le rendu final n'est donc pas énormément démonstratif. Il faudrait, pour réaliser cette lampe et avoir les meilleurs résultats, choisir une lampe frontale plus puissante. 

Malgré ce petit défaut, voici pour finir l'éclairage diffus obtenu avec cette lampe en comparaison avec l'éclairage de base de la lampe frontale. Avec à gauche, un exemple de la lampe posée et, à droite, suspendue.

![Test final](../images/module-4/19_test_final.jpg)

<br>

## **Liens utiles**

- [Télécharger Inkscape](https://inkscape.org/release/inkscape-1.1.1/)
- [Site Lasersaur](https://www.lasersaur.com)
- [Guide Lasersaur](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)
- [Wiki réglages par matériaux Lasersaur](https://github.com/nortd/lasersaur/wiki/materials)
-[Site Epilog](https://www.epiloglaser.com)
- [Guide Epilog Fusion](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/EpilogFusion.md)
- [Wiki réglages par matériaux Epilog](https://github.com/nortd/lasersaur/wiki/materials)
- [Télécharger fichier svg de la grille de calibration découpes et gravures](../images/module-4/calibration_grid.svg)
- [Télécharger fichier svg de la calibration des pliages](../images/module-4/calibration_grid_pliage.svg)
- [Télécharger fichier svg de la lampe](../images/module-4/final-lamp-meg.svg)

***Rappel :** pour télécharger un fichier svg, il faut faire un clic droit sur le lien et "Download linked file"*
